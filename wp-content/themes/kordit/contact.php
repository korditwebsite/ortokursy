<?php
/**
* Template Name: Kontakt
*/
?>
<?php get_header(); ?>
<main id="contact">
	<section class="contact-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-lg-7 wow fadeInLeft">
					<?php if( have_rows('contact_info') ):  while ( have_rows('contact_info') ) : the_row();  ?>
						<h3><?php the_sub_field("title"); ?></h3>
						<p class="contact-desc"><?php the_sub_field("desc"); ?></p>
						<h4><?php the_sub_field("subtitle1"); ?></h4>
						<?php the_sub_field("contact_info"); ?>
						<ul>
							<li>
								<img src="/wp-content/themes/kordit/img/call.png" alt="icon-contact"><a href="tel:<?php the_sub_field("call"); ?>"><?php the_sub_field("call_text"); ?></a>
							</li>
							<li>
								<img src="/wp-content/themes/kordit/img/email.png" alt="icon-contact"><a href="mailto::<?php the_sub_field("mail"); ?>"><?php the_sub_field("mail"); ?></a>
							</li>
						</ul>
						<h4><?php the_sub_field("subtitle2"); ?></h4>
						<?php the_sub_field("transfer"); ?>
					<?php endwhile; else : endif; ?>
				</div>
				<div class="col-lg-5 wow fadeInRight">
					<div class="contact-box">
						<?php echo do_shortcode('[contact-form-7 id="5" title="Formularz 1"]'); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="map-tabs">
		<?php if( have_rows('tabs') ):  while ( have_rows('tabs') ) : the_row();  ?>
			<div class="tab-box">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<ul class="tab">
								<li><button class="tablinks" onclick="openCity(event, 'warszawa')" id="defaultOpen"><?php the_sub_field("city_1"); ?></button></li>
								<li><button class="tablinks" onclick="openCity(event, 'gdansk')"><?php the_sub_field("city_2"); ?></button></li>
								<li><button class="tablinks" onclick="openCity(event, 'krakow')"><?php the_sub_field("city_3"); ?></button></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div id="warszawa" class="tabcontent">
							<?php the_sub_field("city_1_desc"); ?>
						</div>
						<div id="gdansk" style="display: none;" class="tabcontent">
							<?php the_sub_field("city_2_desc"); ?>
						</div>
						<div id="krakow" style="display: none;" class="tabcontent">
							<?php the_sub_field("city_3_desc"); ?>
						</div>
					</div>
				</div>
			</div>
		<?php endwhile; else : endif; ?>
	</section>
</main>
<?php
get_template_part( 'parts/newsletter/newsletter');
?>
<?php get_footer(); ?>