<?php
/**
* Template name: Kategorie
*/
?>
<?php get_header(3); ?>
<main id="category">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        <?php 
        // Check if there are any posts to display
        if ( have_posts() ) : ?>
          
          <header class="archive-header">
            <h1 class="archive-title"> <?php the_archive_title(); ?></h1>
            
            
            <?php
          // Display optional category description
            if ( category_description() ) : ?>
              <div class="archive-meta"><?php echo category_description(); ?></div>
            <?php endif; ?>
          </header>
          
          <?php
          
        // The Loop
          while ( have_posts() ) : the_post(); ?>
            <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
            <small><? echo date('d.m', strtotime($post->post_date)); ?> <? echo date('Y', strtotime($post->post_date)); ?> Autor: <?php the_author_posts_link() ?></small>
            
            <div class="entry">
              <?php the_content(); ?>
            </div>
            
          <?php endwhile;
          
          else: ?>
            <p>Sorry, no posts matched your criteria.</p>
            
            
          <?php endif; ?>
        </div>
        <div class="col-lg-4">
          <div class="sidebar">
            <?php dynamic_sidebar('sidebarblog'); ?>
            <div class="sign-up">
              <h3><?php the_field('signup_sidebar_title', pll_current_language('slug')); ?></h3>
              <p><?php the_field('signup_sidebar_desc', pll_current_language('slug')); ?></p>
              <a href="<?php the_field('signup_sidebar_link', pll_current_language('slug')); ?>"><?php the_field('signup_sidebar_text', pll_current_language('slug')); ?></a>
            </div>
            <div class="news-post">
              <h4>Zobacz również</h4>
              <?php
              query_posts($query_string);
              if ( get_query_var( 'paged' ) ) {
                $paged = get_query_var( 'paged' );
              } elseif ( get_query_var( 'page' ) ) {
                $paged = get_query_var( 'page' );
              } else {
                $paged = 1;
              }
              $custom_args = array(
                'post_status'   => 'publish',
                'orderby'       =>  'date',
                'order'         =>'DESC',
                'posts_per_page'=>3,
                'paged' => $paged
              );
              $wp_query = new WP_Query( $custom_args );
              $getPosts =  get_posts($custom_args);
              ?>
              <?php if ($getPosts) : ?>
                <?php global $post,$wp_query;?>
                <?php foreach ($getPosts as $post): ?>
                  <?php setup_postdata($post);?>
                  <div class="simple-post">
                    <div class="post-loop">
                      <img src="<?php echo get_the_post_thumbnail_url($post) ?>">
                    </div>
                    <div class="row news-title">
                      <h5><? echo $post->post_title; ?></h5>
                      <a class="read-more" style="width: 100%;" href="<?php echo get_permalink($post,false) ?>"><?php echo $readmorebutton; ?></a>
                    </div>
                  </div>
                <?php endforeach;?>
                <?php
              else :
              endif;
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
  <section class="question">
    <div class="container question-wrapper">
      <div class="row">
        <?php if( have_rows('question_wrapper', pll_current_language('slug')) ):  while ( have_rows('question_wrapper', pll_current_language('slug')) ) : the_row();  ?>
          <div class="col-lg-12 ">
            <h4><?php the_sub_field("question_title", pll_current_language('slug')); ?></h4>
            <?php the_sub_field("question_desc", pll_current_language('slug')); ?>
            <ul>
              <li><a href="mailto:<?php the_sub_field("question_mail", pll_current_language('slug')); ?>"><img src="/wp-content/themes/kordit/img/cta-mail.png"><?php the_sub_field("question_mail", pll_current_language('slug')); ?></a></li>
              <li><img src="/wp-content/themes/kordit/img/cta-call.png"><a href="tel:+48<?php the_sub_field("question_call", pll_current_language('slug')); ?>"><?php the_sub_field("question_call_text", pll_current_language('slug')); ?></a></li>
            </ul>
          </div>
        <?php endwhile; else : endif; ?>
      </div>
    </div>
  </section>
  <?php get_footer(); ?>