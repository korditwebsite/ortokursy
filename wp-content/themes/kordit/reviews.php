<?php
/**
* Template Name: Opinie
*/
?>
<?php get_header(); ?>
<main id="reviews">
	<section class="reviews">
		<div class="container">
			<div class="row">
				<?php if( have_rows('reviews_box') ):  while ( have_rows('reviews_box') ) : the_row();  ?>
				<div class="col-xl-12 reviews-box wow zoomIn">
					<div class="reviews-box-inner">
						<p><?php the_sub_field("reviews"); ?></p>
						<span> - <?php the_sub_field("person"); ?></span>
					</div>
					<div class="triangle"></div>
				</div>
				<?php endwhile; else : endif; ?>
			</div>
		</div>
	</section>
</main>
<?php
get_template_part( 'parts/question/question');
?>
<?php get_footer(); ?>