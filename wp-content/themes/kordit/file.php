<?php
/**
* Template Name: Materiały dla uczestników
*/
?>
<?php get_header(); ?>
<main id="file">
	<section class="file">
		<div class="container">
			<div class="row">
				<?php if( have_rows('pdf') ):  while ( have_rows('pdf') ) : the_row();  ?>
					<div class="col-lg-6 file-wrapper">
						<div class="file-box">
							<div class="file-box-pdf">
								<img src="/wp-content/themes/kordit/img/pdf.png" alt="icon-pdf" class="pdf-icon">
								<h3><?php the_sub_field("pdf_name"); ?></h3>
							</div>
							<a href="<?php the_sub_field("pdf_file"); ?>" target="_blank"><?php echo $download; ?> <img src="/wp-content/themes/kordit/img/download.png"></a>
						</div>
					</div>
				<?php endwhile; else : endif; ?>
				<?php if( have_rows('doc') ):  while ( have_rows('doc') ) : the_row();  ?>
					<div class="col-lg-6 file-wrapper">
						<div class="file-box">
							<h3><?php the_sub_field("doc_name"); ?></h3>
							<a href="<?php the_sub_field("doc_file"); ?>" target="_blank"><?php echo $download; ?> <img src="/wp-content/themes/kordit/img/download.png"></a>
						</div>
					</div>
				<?php endwhile; else : endif; ?>
			</div>
		</div>
	</section>
</main>
<?php
get_template_part( 'parts/question/question');
?>
<?php get_footer(); ?>