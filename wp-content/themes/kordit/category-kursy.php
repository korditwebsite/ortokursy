<?php
/**
* Template Name: Kursy kategorie
*/
?>
<?php get_header(); ?>

<main id="oferta-single">
	<div class="container">
<h3><?php the_title(); ?></h3> 
<?php the_post_thumbnail( 'malyprostokat', "", array( "class" => "img-fluid", "alt" => "logo" ) ); ?>
		</div>

		<?php echo the_terms( $post->ID, 'topics', 'Topics: ', ', ', ' ' ); ?>

	</main>
	<?php get_footer(); ?>