<?php
/**
Template Name: blog
Template Post Type: post
*/
?>
<?php get_header(3); ?>
<main id="blog-post">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<article>
					<div class="thumbnail">
						<h3 class="title-section"><?php echo short_filter_wp_title( $title ); ?></h3>
						<div class="row blog-params">
							<p><? echo date('d.m', strtotime($post->post_date)); ?> <? echo date('Y', strtotime($post->post_date)); ?></p>
							<span> / </span>
							<p>Autor: <div class="bold"> <?php the_author(); ?></div></p>
							<span> / </span>
							<p><?php the_category( $separator, $parents, $post_id ); ?> </p>
						</div>
						<?php
						if ( has_post_thumbnail()  ) {
							the_post_thumbnail( 'home-thumbnail' );
						}
						?>
					</div>
					<div class="textbox">
						<?php // check if the flexible content field has rows of data
						if (get_the_content()) {
							the_content();
						}
						else{


						if( have_rows('pojedyncza_sekcja') ):
						// loop through the rows of data
							while ( have_rows('pojedyncza_sekcja') ) : the_row();
						//textbox
								if( get_row_layout() == 'textbox' ):
									get_template_part( 'templates/textbox');
						//Za i przeciw
								elseif( get_row_layout() == 'comparison' ):
									get_template_part( 'templates/comparison', $numersekcji);
						//Zapisz się
								elseif( get_row_layout() == 'post_signup' ):
									get_template_part( 'templates/post_signup', $numersekcji);
						//Literatura
								elseif( get_row_layout() == 'literature' ):
									get_template_part( 'templates/literature', $numersekcji);
								endif;
							endwhile;
						else :
						// no layouts found
						endif;
					}
						?>
					</div>
					<div class="row biography-box">
						<div class="col-md-5 biography-img">
							<?php
							echo get_avatar( $id_or_email, $size = '250', $default = '<path_to_url>' );
							?>
						</div>
						<div class="col-md-5 biography">
							<?php echo nl2br(get_the_author_meta('description')); ?>
						</div>
					</div>
					
				</article>
			</div>
			<div class="col-md-4">
				<div class="sidebar">
					<?php dynamic_sidebar('sidebarblog'); ?>
					<div class="sign-up">
						<h3><?php the_field('signup_sidebar_title', pll_current_language('slug')); ?></h3>
						<p><?php the_field('signup_sidebar_desc', pll_current_language('slug')); ?></p>
						<a href="<?php the_field('signup_sidebar_link', pll_current_language('slug')); ?>"><?php the_field('signup_sidebar_text', pll_current_language('slug')); ?></a>
					</div>
					<div class="news-post">
						<h4>Zobacz również</h4>
						<?php
						query_posts($query_string);
						if ( get_query_var( 'paged' ) ) {
							$paged = get_query_var( 'paged' );
						} elseif ( get_query_var( 'page' ) ) {
							$paged = get_query_var( 'page' );
						} else {
							$paged = 1;
						}
						$custom_args = array(
							'post_status'   => 'publish',
							'orderby'       =>  'date',
							'order'         =>'DESC',
							'posts_per_page'=>3,
							'paged' => $paged
						);
						$wp_query = new WP_Query( $custom_args );
						$getPosts =  get_posts($custom_args);
						?>
						<?php if ($getPosts) : ?>
							<?php global $post,$wp_query;?>
							<?php foreach ($getPosts as $post): ?>
								<?php setup_postdata($post);?>
								<div class="simple-post">
									<div class="post-loop">
										<img src="<?php echo get_the_post_thumbnail_url($post) ?>">
									</div>
									<div class="row news-title">
										<h5><? echo $post->post_title; ?></h5>
										<a class="read-more" style="width: 100%;" href="<?php echo get_permalink($post,false) ?>"><?php echo $readmorebutton; ?></a>
									</div>
								</div>
							<?php endforeach;?>
							<?php
						else :
						endif;
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?php get_footer(); ?>