<?php 
/* Template Name: Fizjoterapeuci */ 
get_header(); 
?>
<main id="fizjo">
    <div class="container-map">
        <div class="container mb-5">
            <div class="row top-fizjo">
                <div class="col-xl-4">
                    <div class="inner-top-fizjo">
                        <h2><?php the_field( 'tytul' ); ?></h2>
                        <img src="/wp-content/uploads/2019/07/Warstwa-17.png">
                    </div>
                </div>
                <div class="col-xl-8">
                    <div class="inner-top-fizjo">
                        <?php the_field('opis'); ?>
                    </div>
                </div>
            </div>
            <form method="GET" action="">
                <div class="row">
                    <div class="col-lg-4">
                        <label for="full_name">Imię i nazwisko</label>
                        <input type="text" class="form-control" id="full_name" name="full_name" placeholder="Wpisz imię i nazwisko" <?php if (isset($_GET["full_name"])) { echo 'value="'. $_GET["full_name"] .'"';} ?>>
                    </div>
                    <div class="col-lg-4">
                        <label for="voivodeship"><?php echo $wojewodztwo; ?></label>
                        <select class="form-control" id="voivodeship" name="voivodeship">
                            <option selected disabled>Wybierz województwo</option>
                            <?php
                            $terms = get_terms(
                                array(
                                    'taxonomy' => 'wojewodztwo',
                                    'hide_empty' => false,
                                    'parent'   => 0
                                )
                            );

                            foreach ($terms as $term) { 
                                ?>
                                <option <?php if($term->slug == $_GET["voivodeship"]){echo 'selected';}?> value="<?php echo $term->slug;?>"><?php echo $term->name;?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-lg-4">
                        <label for="city">Miasto</label>
                        <input type="text" class="form-control" id="city" name="city" placeholder="Wpisz nazwę miasta" <?php if (isset($_GET["city"])) { echo 'value="'. $_GET["city"] .'"';} ?>>
                    </div>
                </div>
                <div class="row mt-3 align-items-end d-flex">
                    <div class="col-lg-4">
                        <label for="course">Kurs</label>
                        <input type="text" class="form-control" id="course" name="course" placeholder="Wpisz kurs ukończony przez fizjoterapeutę" <?php if (isset($_GET["course"])) { echo 'value="'. $_GET["course"] .'"';} ?>>
                    </div>
                    <div class="col-lg-2 ml-auto">
                        <button type="submit" id="filter" class="btn btn-primary w-100"><?php echo $find; ?></button>
                    </div>
                </div>
            </form>
        </div>
        <div class="container">
            <div class="popup-nav" style="height: 500px; width:100%;">
                <div class="inner-container-map">
                    <div id="map_canvas">
                        <noscript>
                            <p>JavaScript is required to render the Google Map.</p>
                        </noscript>
                    </div>
                    <h5><?php echo $ladowanie_mapy; ?></h5>
                </div>
            </div>
        </div>
        <div class="container-person">
            <div class="container">
                <h3 class="mb-3"><?php echo $wyniki_wyszukiwania; ?>:</h3>
                <div class="row">
                    <?php 
                    if (get_query_var('paged')) {
                        $paged = get_query_var('paged');
                    } elseif (get_query_var('page')) {
                        $paged = get_query_var('page');
                    } else {
                        $paged = 1;
                    }
                    
                    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
                        $filter_name = htmlspecialchars($_GET["full_name"]);
                        $filter_voivodeship = htmlspecialchars($_GET["voivodeship"]);
                        $filter_city = htmlspecialchars($_GET["city"]);
                        $filter_course = htmlspecialchars($_GET["course"]);
                        
                        if (!empty($filter_city) && !empty($filter_voivodeship)) {
                            $filter_args_tq = array( array( 'taxonomy' => 'wojewodztwo', 'field' => 'slug', 'terms' => array($filter_voivodeship, $filter_city), 'operator' => 'IN' ), );
                        } elseif (!empty($filter_city)) {
                            $filter_args_tq = array( array( 'taxonomy' => 'wojewodztwo', 'field' => 'slug', 'terms' => $filter_city, 'operator' => 'IN' ), );
                        } elseif (empty($filter_city) && empty(!$filter_voivodeship)) {
                            $filter_args_tq = array( array( 'taxonomy' => 'wojewodztwo', 'field' => 'slug', 'terms' => $filter_voivodeship, 'operator' => 'IN' ), );
                        } elseif (empty($filter_city) && empty($filter_voivodeship)) {
                            $filter_args_tq = 'any';
                        }
                        
                        if (!empty($filter_course)) {
                            $filter_args_mq = array( array( 'key' => 'zglos_kursy', 'value' => $filter_course, 'compare' => 'LIKE', ) );
                        } else {
                            $filter_args_mq = '';
                        }
                        
                        if (!empty($filter_name)) {
                            $filter_args_s = $filter_name;
                        } else {
                            $filter_args_s = '';
                        }
                    }
                    
                    $args = array(
                        'post_type' => 'fizjoterapeuci',                                
                        'order' => 'ASC',
                        'posts_per_page' => '4',
                        'paged' => $paged,
                        's' => $filter_args_s,
                        'tax_query' => $filter_args_tq,
                        'meta_query' => $filter_args_mq
                    );

                    $wp_query = new WP_Query($args);
                    $loop = new WP_Query($args);
                    $i = 0;
                    while ($loop->have_posts()) : $loop->the_post();

                        $terms = get_the_terms($post->ID, 'wojewodztwo');
                        foreach ($terms as $term){
                            if ($term->parent == 0) {
                                $ad_voivodeship = $term->name;
                            } elseif ($term->parent != 0) {
                                $ad_city = $term->name;
                            }
                        }

                        $ad_phone = get_field('zglos_phone');
                        $ad_mail = get_field('zglos_email'); 
                        $ad_clinic_name = get_field('zglos_place'); 
                        $ad_adress = get_field('zglos_address');
                        $ad_course = get_field('zglos_kursy');
                        ?>
                        <div class="col-xl-6 col-md-6 item-doctor">
                            <div class="row">
                                <div class="col-xl-6">
                                    <?php the_post_thumbnail('malyprostokat', "", array("class" => "img-fluid", "alt" => "Zdjęcie fizjoterapeuty")); ?>
                                    <div class="border-contact">
                                        <span class="mail">
                                            <img src="/wp-content/uploads/2019/07/mobile.png" alt="image mobile">
                                            <a href="tel:<?php echo $ad_phone;?>"><?php echo $ad_phone;?></a>
                                        </span>
                                        <span class="tel">
                                            <img src="/wp-content/uploads/2019/07/mail.png" alt="image mobile">
                                            <a href="mailto:<?php echo $ad_mail;?>"><?php echo $ad_mail;?></a>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="inner-item-person">
                                        <h4><?php echo the_title();?></h4>
                                        <span><b><?php echo $wojewodztwo; ?>:</b> <?php echo $ad_voivodeship;?></span>
                                        <span><b><?php echo $miasto; ?>:</b> <?php echo $ad_city;?></span>
                                        <span><b><?php echo $placowka; ?>:</b> <?php echo $ad_clinic_name;?></span>
                                        <span><b><?php echo $adres; ?>:</b> <?php echo $ad_adress;?></span>
                                        <span class="courses"><b>Ukończone kursy:</b> <?php echo $ad_course;?></span>
                                        <a href="#map_canvas">
                                            <span class="look-more look-more-<?php echo $i;?>"><?php echo $zobacz_na_mapie; ?></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php $i++; endwhile; wp_reset_postdata();?>
                    </div>
                </div>
                <div class="pagin">
                    <?php 
                    if (custom_pagination($wp_query->max_num_pages, "", $paged)) {
                       if (function_exists(custom_pagination)) {
                        custom_pagination($wp_query->max_num_pages, "", $paged);
                    } 
                }
                ?>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript">
    function initialise() {
        var myOptions = {
            zoom: 8
            ,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
        var bounds = new google.maps.LatLngBounds();

        <?php
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $filter_name = htmlspecialchars($_GET["full_name"]);
            $filter_voivodeship = htmlspecialchars($_GET["voivodeship"]);
            $filter_city = htmlspecialchars($_GET["city"]);
            $filter_course = htmlspecialchars($_GET["course"]);

            if (!empty($filter_city) && !empty($filter_voivodeship)) {
                $filter_args_tq = array( array( 'taxonomy' => 'wojewodztwo', 'field' => 'slug', 'terms' => array($filter_voivodeship, $filter_city), 'operator' => 'IN' ), );
            } elseif (!empty($filter_city)) {
                $filter_args_tq = array( array( 'taxonomy' => 'wojewodztwo', 'field' => 'slug', 'terms' => $filter_city, 'operator' => 'IN' ), );
            } elseif (empty($filter_city) && empty(!$filter_voivodeship)) {
                $filter_args_tq = array( array( 'taxonomy' => 'wojewodztwo', 'field' => 'slug', 'terms' => $filter_voivodeship, 'operator' => 'IN' ), );
            } elseif (empty($filter_city) && empty($filter_voivodeship)) {
                $filter_args_tq = 'any';
            }

            if (!empty($filter_course)) {
                $filter_args_mq = array( array( 'key' => 'zglos_kursy', 'value' => $filter_course, 'compare' => 'LIKE', ) );
            } else {
                $filter_args_mq = '';
            }

            if (!empty($filter_name)) {
                $filter_args_s = $filter_name;
            } else {
                $filter_args_s = '';
            }
        }

        $args = array(
            'post_type' => 'fizjoterapeuci',                                
            'order' => 'ASC',
            'posts_per_page' => '10',
            'paged' => $paged,
            's' => $filter_args_s,
            'tax_query' => $filter_args_tq,
            'meta_query' => $filter_args_mq
        );

        $wp_query = new WP_Query($args);
        $loop = new WP_Query($args);
        while ($loop->have_posts()) : $loop->the_post();

            $ad_phone = get_field('zglos_phone');
            $ad_mail = get_field('zglos_email');
            $ad_adress = get_field('zglos_address');
            $ad_clinic_name = get_field('zglos_place');

            $location = get_field('lokalizacja');
            $location_vlat = $location['lat'];
            $location_vlng = $location['lng'];

            $locations_vlat[] = $location_vlat;
            $locations_vlng[] = $location_vlng;

            $locations[] = '<div class="popup-map"><h3>'. get_the_title() .'</h3><p>'. $ad_adress .'</p><span class="tel-popup"><img src="/wp-content/uploads/2019/07/mobile.png" alt="image mobile"><a href="tel:'. $ad_phone .'">'. $ad_phone .'</a></span><span class="mail-popup"><img src="/wp-content/uploads/2019/07/mail.png" alt="image mail"><a href="mailto:'. $ad_mail .'">'. $ad_mail .'</a></span></div>';

        endwhile; wp_reset_postdata();
        ?>

        var locations = [
        <?php 
        $i = 0;
        foreach ($locations as $location) {
            echo "['" . $location . "',". $locations_vlat[$i] .", ". $locations_vlng[$i] ."],";
            $i++;
        }
        ?>
        ];

        for (var i = 0; i < locations.length; i++) {
            var location = locations[i];
            var position = new google.maps.LatLng(location[1], location[2]);
            bounds.extend(position);
            var marker = new google.maps.Marker({
                animation: google.maps.Animation.DROP,
                icon: "/wp-content/uploads/2019/07/pin.png",
                map: map,
                position: position,
                title: location[0]
            });

            <?php 
            $i = 0;
            foreach ($locations as $location) {
                echo
                "
                $('.look-more-". $i ."').click(function() {
                    window.setTimeout(function() {
                        var przescrolluj = new google.maps.LatLng(". $locations_vlat[$i] .", ". $locations_vlng[$i] .");
                        map.panTo(przescrolluj);
                    }, 3000);
                    map.setZoom(15);
                    map.setCenter(przescrolluj);
                });
                ";
                $i++;
            }
            ?>

            google.maps.event.addListener(marker, 'click', (
                function(marker, i) {
                    return function() {
                        var infowindow = new google.maps.InfoWindow();
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                }
                )(marker, i));
        };
        map.fitBounds(bounds);
    }
    function loadScript() {
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.src = "http://maps.googleapis.com/maps/api/js?key=AIzaSyDElKd9kJ9hLcTX8gfCTAmg2UghCIsAZ3A&callback=initMap?&callback=initialise";
        document.body.appendChild(script);
    }
    window.onload = loadScript;
</script>
<section class="question">
    <div class="container question-wrapper">
        <div class="row">
            <?php if( have_rows('question_wrapper', pll_current_language('slug')) ):  while ( have_rows('question_wrapper', pll_current_language('slug')) ) : the_row();  ?>
                <div class="col-lg-12 ">
                    <h4><?php the_sub_field("question_title", pll_current_language('slug')); ?></h4>
                    <?php the_sub_field("question_desc", pll_current_language('slug')); ?>
                    <ul>
                        <li><a href="mailto:<?php the_sub_field("question_mail", pll_current_language('slug')); ?>"><img src="/wp-content/themes/kordit/img/cta-mail.png"><?php the_sub_field("question_mail", pll_current_language('slug')); ?></a></li>
                        <li><img src="/wp-content/themes/kordit/img/cta-call.png"><a href="tel:+48<?php the_sub_field("question_call", pll_current_language('slug')); ?>"><?php the_sub_field("question_call_text", pll_current_language('slug')); ?></a></li>
                    </ul>
                </div>
            <?php endwhile; else : endif; ?>
        </div>
    </div>
</section>
<?php get_footer(); ?>