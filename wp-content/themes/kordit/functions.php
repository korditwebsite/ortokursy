<?php
//Inicjacja Sidebar
function quality_construction_widgets_init()
{
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'quality-construction'),
        'id' => 'sidebar-1',
        'description' => esc_html__('Add widgets here.', 'quality-construction'),
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
    register_sidebar( array(
        'name'          => 'sidebar-blog',
        'id'            => 'sidebarblog',
        'before_widget' => '<div id="%1$s" class="widget-item">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="">',
        'after_title'   => '</h4>',
    ) );
}
add_action('widgets_init', 'quality_construction_widgets_init');
//Rozmiar obrazków
add_theme_support('post-thumbnails');
add_image_size( 'malyprostokat', 400, 300 );
add_image_size( 'o-nas', 540, 320 );
add_image_size( 'kontener', 1100, 630 );
add_image_size( 'big', 1024, 1024 );
add_image_size( 'full-gallery', 1920, 780 );
add_image_size( 'full', 1920, 1080 );
add_image_size( 'paralax-home', 680, 1110 );
add_image_size( 'gallery', 320, 180 );
add_image_size( 'gallery-home', 500, 400);


add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

function pagination() {

    global $wp_query;

    if ($wp_query->max_num_pages > 1) { echo '<p class="pages" role="navigation">' . paginate_links( array(
        'base' => @add_query_arg('paged','%#%'),
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var('paged') ),
        'total' => $wp_query->max_num_pages,
        'prev_text' => __('« '),
        'next_text'    => __(' »'),
    ) ) . '</p>'; }
}
// Pagination
function custom_pagination($numpages = '', $pagerange = '', $paged='') {
    if (empty($pagerange)) {
        $pagerange = 2;
    }
/*
* This first part of our function is a fallback
* for custom pagination inside a regular loop that
* uses the global $paged and global $wp_query variables.
*
* It's good because we can now override default pagination
* in our theme, and use this function in default queries
* and custom queries.
*/
global $paged;
if (empty($paged)) {
    $paged = 1;
}
if ($numpages == '') {
    global $wp_query;
    $numpages = $wp_query->max_num_pages;
    if(!$numpages) {
        $numpages = 1;
    }
}
function my_avatar_filter() {
// Remove from show_user_profile hook
    remove_action('show_user_profile', array('wp_user_avatar', 'wpua_action_show_user_profile'));
    remove_action('show_user_profile', array('wp_user_avatar', 'wpua_media_upload_scripts'));
// Remove from edit_user_profile hook
    remove_action('edit_user_profile', array('wp_user_avatar', 'wpua_action_show_user_profile'));
    remove_action('edit_user_profile', array('wp_user_avatar', 'wpua_media_upload_scripts'));
// Add to edit_user_avatar hook
    add_action('edit_user_avatar', array('wp_user_avatar', 'wpua_action_show_user_profile'));
    add_action('edit_user_avatar', array('wp_user_avatar', 'wpua_media_upload_scripts'));
}
// Loads only outside of administration panel
if(!is_admin()) {
    add_action('init','my_avatar_filter');
}
if (is_page_template( 'templates/blog.php' )) {
    do_action('edit_user_avatar', $current_user);
}
/**
* We construct the pagination arguments to enter into our paginate_links
* function. 
*/

$pagination_args = array(
    'base'           =>add_query_arg('page','%#%'),
    'format'          => 'page/%#%',
    'total'           => $numpages,
    'current'         => $paged,
    'show_all'        => False,
    'end_size'        => 1,
    'mid_size'        => $pagerange,
    'prev_next'       => True,
    'prev_text'       => __('<'),
    'next_text'       => __('>'),
    'type'            => 'plain',
    'add_args'        => false,
    'add_fragment'    => ''
);
$paginate_links = paginate_links($pagination_args);
if ($paginate_links) {
    echo "<nav class='custom-pagination'>";
    //echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
    echo $paginate_links;
    echo "</nav>";
}
}
//Inicjacja plików motywu
function theme_enqueue_scripts() {
    wp_enqueue_style( 'Bootstrap_css', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );
    wp_enqueue_style( 'animate', get_template_directory_uri() . '/assets/animate.css' );
    wp_enqueue_style( 'OwlCarousel', get_template_directory_uri() . '/assets/owlcarousel/owl.carousel.min.css' );
    wp_enqueue_style( 'OwlCarouselDefault', get_template_directory_uri() . '/assets/owlcarousel/owl.theme.default.min.css' );
    wp_enqueue_style( 'Style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_script( 'jQuery', get_template_directory_uri() . '/assets/js/jquery-3.3.1.min.js', array(), '3.3.1', true );
    wp_enqueue_script( 'owlCarousel', get_template_directory_uri() . '/assets/owlcarousel/owl.carousel.min.js', array(), '2.3.4', true );
    wp_enqueue_script( 'Bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'gallery', get_template_directory_uri() . '/scripts/gallery.js', array());
    wp_enqueue_script( 'apear', get_template_directory_uri() . '/scripts/apear.js', array());
    wp_enqueue_script( 'Main', get_template_directory_uri() . '/scripts/main.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts' );
//Logo img
if (!function_exists('quality_construction_custom_logo_setup')) :
    function quality_construction_custom_logo_setup()
    {
        add_theme_support('custom-logo', array(
            'height' => 120,
            'width' => 480,
            'flex-width' => true,
        ));
    }
    add_action('after_setup_theme', 'quality_construction_custom_logo_setup');
endif;

//Skrócony tytuł
function short_filter_wp_title( $title ) {
    if ( is_single() || ( is_home() && !is_front_page() ) || ( is_page() && !is_front_page() ) ) {
        $title = single_post_title( '', false );
    }
    if ( is_front_page() && ! is_page() ) {
        $title = esc_attr( get_bloginfo( 'name' ) );
    }
    return $title;
    add_filter( 'wp_title', 'short_filter_wp_title', 100);
}
//Wsparcie dla breadcrumbs
add_theme_support( 'yoast-seo-breadcrumbs' );
//Wsparcie dla admin bar
function admin_bar(){
    if(is_user_logged_in()){
        add_filter( 'show_admin_bar', '__return_true' , 1000 );
    }
}
add_action('init', 'admin_bar' );

/** ===============Rejestracja CPT 2 ==================**/


add_action('init', 'kursy_register');
function kursy_register() {

  $labels = array(
    'name' => _x('Kursy', 'post type general name'),
    'singular_name' => __( 'kursy' ),
    'add_new' => _x('Add New', 'Dodaj kurs'),
    'add_new_item' => __('Dodaj kurs'),
    'edit_item' => __('Edytuj kurs'),
    'new_item' => __('Dodaj kurs'),
    'view_item' => __('Zobacz kurs'),
    'search_items' => __('Znajdź kurs'),
    'not_found' =>  __('Nie znaleziono kursu'),
    'not_found_in_trash' => __('Nic nie znaleziono w koszu'),
    'add_new' => __( 'Dodaj nowy kurs' ),
    // 'taxonomies'          => array('topics', 'category' ),
    
);
  
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => array('title','editor','thumbnail'),
    'post_id' => 'grafika',
    'position' => "2",
    'menu_icon'   => 'dashicons-admin-home',
    'post_type' => 'kursy'
); 

  register_post_type( 'kursy' , $args );
}

/** ===============Rejestracja CPT 3 ==================**/


add_action('init', 'fizjoterapeuci_register');
function fizjoterapeuci_register() {

  $labels = array(
    'name' => _x('Fizjoterapeuci', 'post type general name'),
    'singular_name' => __( 'Fizjoterapeuci' ),
    'add_new' => _x('Add New', 'Dodaj fizjoterapeutę'),
    'add_new_item' => __('Dodaj fizjoterapeutę'),
    'edit_item' => __('Edytuj fizjoterapeutę'),
    'new_item' => __('Dodaj fizjoterapeutę'),
    'view_item' => __('Zobacz fizjoterapeutę'),
    'search_items' => __('Znajdź fizjoterapeutę'),
    'not_found' =>  __('Nie znaleziono kursu'),
    'not_found_in_trash' => __('Nic nie znaleziono w koszu'),
    'add_new' => __( 'Dodaj nowego fizjoterapeutę' ),
    // 'taxonomies'          => array('topics', 'category' ),
    
);
  
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => array('title','editor','thumbnail'),
    'post_id' => 'grafika',
    'position' => "2",
    'menu_icon'   => 'dashicons-universal-access',
    'post_type' => 'fizjoterapeuci'
); 

  register_post_type( 'fizjoterapeuci' , $args );
}


/** ===============Rejestracja CPT END ==================**/

// Taksomia typy
add_action( 'init', 'create_kursy_custom_taxonomy', 0 );

//create a custom taxonomy name it "type" for your posts
function create_kursy_custom_taxonomy() {

  $labels = array(
    'name' => _x( 'Typy', 'taxonomy general name' ),
    'singular_name' => _x( 'Typy kursów', 'taxonomy singular name' ),
    'search_items' =>  __( 'Wyszukaj typ' ),
    'all_items' => __( 'Wszystkie typy kursu' ),
    'parent_item' => __( 'Rodzic' ),
    'parent_item_colon' => __( 'Rodzic:' ),
    'edit_item' => __( 'Edytuj typ kursu' ), 
    'update_item' => __( 'Uaktualnij typ' ),
    'add_new_item' => __( 'Dodaj typ kursu' ),
    'new_item_name' => __( 'Nowy typ kursu' ),
    'menu_name' => __( 'Typ kursu' ),
);    

  register_taxonomy('types',array('kursy'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'type' ),
));
}

// Taksomia popularne
add_action( 'init', 'create_kursy_popular_custom_taxonomy', 0 );

//create a custom taxonomy name it "popular" for your posts
function create_kursy_popular_custom_taxonomy() {

  $labels = array(
    'name' => _x( 'Popularne', 'taxonomy general name' ),
    'singular_name' => _x( 'Popularne kursy', 'taxonomy singular name' ),
    'search_items' =>  __( 'Wyszukaj typ' ),
    'all_items' => __( 'Wszystkie popularne kursy' ),
    'parent_item' => __( 'Rodzic' ),
    'parent_item_colon' => __( 'Rodzic:' ),
    'edit_item' => __( 'Edytuj popularne kursy' ), 
    'update_item' => __( 'Uaktualnij popularne' ),
    'add_new_item' => __( 'Dodaj popularne kursy' ),
    'new_item_name' => __( 'Nowe popularne kursy' ),
    'menu_name' => __( 'Popularne kursy' ),
);    

  register_taxonomy('popular',array('kursy'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'popular' ),
));
}


// Taksomia popularne
add_action( 'init', 'create_kursy_popular_custom_taxonomy', 0 );

//create a custom taxonomy name it "wojewodztwo" for your posts
function create_fizjoterapeuci_wojewodztwo_custom_taxonomy() {

  $labels = array(
    'name' => _x( 'Województwo', 'taxonomy general name' ),
    'singular_name' => _x( 'Województwo', 'taxonomy singular name' ),
    'search_items' =>  __( 'Wyszukaj województwo' ),
    'all_items' => __( 'Wszystkie województwa' ),
    'parent_item' => __( 'Rodzic' ),
    'parent_item_colon' => __( 'Rodzic:' ),
    'edit_item' => __( 'Edytuj województwo' ), 
    'update_item' => __( 'Uaktualnij województwo' ),
    'add_new_item' => __( 'Dodaj województwo/miasto' ),
    'new_item_name' => __( 'Nowe województwo' ),
    'menu_name' => __( 'Województwo / miasto' ),
);    

  register_taxonomy('wojewodztwo',array('fizjoterapeuci'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'wojewodztwo' ),
));
}

add_action( 'init', 'create_fizjoterapeuci_wojewodztwo_custom_taxonomy', 0 );



$languages = array( 'pl', 'en' );
foreach ( $languages as $lang ) {
    acf_add_options_page( array(
      'page_title' => 'Site Options (' . strtoupper( $lang ) . ')',
      'menu_title' => __('Site Options (' . strtoupper( $lang ) . ')', 'text-domain'),
      'menu_slug'  => "site-options-${lang}",
      'post_id'    => $lang
  ) );
}

function stf_redirect_to_post(){
    global $wp_query;

    // If there is one post on archive page
    if( is_archive() && $wp_query->post_count == 1 ){
        // Setup post data
        the_post(); 
        // Get permalink
        $post_url = get_permalink();
        // Redirect to post page
        wp_redirect( $post_url );
    }   

} add_action('template_redirect', 'stf_redirect_to_post');



//Dodanie miniaturek do strony
add_theme_support( 'post-thumbnails' );
//Nawigacja nav-walker
class bs4Navwalker extends Walker_Nav_Menu
{
/**
* Starts the list before the elements are added.
*
* @see Walker::start_lvl()
*
* @since 3.0.0
*
* @param string $output Passed by reference. Used to append additional content.
* @param int    $depth  Depth of menu item. Used for padding.
* @param array  $args   An array of arguments. @see wp_nav_menu()
*/
public function start_lvl( &$output, $depth = 0, $args = array() ) {
    $indent = str_repeat("\t", $depth);
    $output .= "\n$indent<div class=\"dropdown-menu\">\n";
}
    /**
    * Ends the list of after the elements are added.
    *
    * @see Walker::end_lvl()
    *
    * @since 3.0.0
    *
    * @param string $output Passed by reference. Used to append additional content.
    * @param int    $depth  Depth of menu item. Used for padding.
    * @param array  $args   An array of arguments. @see wp_nav_menu()
    */
    public function end_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</div>\n";
    }
/**
* Start the element output.
*
* @see Walker::start_el()
*
* @since 3.0.0
*
* @param string $output Passed by reference. Used to append additional content.
* @param object $item   Menu item data object.
* @param int    $depth  Depth of menu item. Used for padding.
* @param array  $args   An array of arguments. @see wp_nav_menu()
* @param int    $id     Current item ID.
*/
public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
    $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
    $classes = empty( $item->classes ) ? array() : (array) $item->classes;
    $classes[] = 'menu-item-' . $item->ID;
/**
* Filter the CSS class(es) applied to a menu item's list item element.
*
* @since 3.0.0
* @since 4.1.0 The `$depth` parameter was added.
*
* @param array  $classes The CSS classes that are applied to the menu item's `<li>` element.
    * @param object $item    The current menu item.
    * @param array  $args    An array of {@see wp_nav_menu()} arguments.
    * @param int    $depth   Depth of menu item. Used for padding.
    */
$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
    // New
$class_names .= ' nav-item';

if (in_array('menu-item-has-children', $classes)) {
    $class_names .= ' dropdown';
}
if (in_array('current-menu-item', $classes)) {
    $class_names .= ' active';
}
    //
$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
    // print_r($class_names);
    /**
    * Filter the ID applied to a menu item's list item element.
    *
    * @since 3.0.1
    * @since 4.1.0 The `$depth` parameter was added.
    *
    * @param string $menu_id The ID that is applied to the menu item's `<li>` element.
        * @param object $item    The current menu item.
        * @param array  $args    An array of {@see wp_nav_menu()} arguments.
        * @param int    $depth   Depth of menu item. Used for padding.
        */
    $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args, $depth );
    $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';
        // New
    if ($depth === 0) {
        $output .= $indent . '<li' . $id . $class_names .'>';
    }
            //
            // $output .= $indent . '<li' . $id . $class_names .'>';
    $atts = array();
    $atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
    $atts['target'] = ! empty( $item->target )     ? $item->target     : '';
    $atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
    $atts['href']   = ! empty( $item->url )        ? $item->url        : '';
                // New
    if ($depth === 0) {
        $atts['class'] = 'nav-link';
    }
    if ($depth === 0 && in_array('menu-item-has-children', $classes)) {
        $atts['class']       .= ' dropdown-toggle';
        $atts['data-toggle']  = 'dropdown';
    }
    if ($depth > 0) {
        $manual_class = array_values($classes)[0] .' '. 'dropdown-item';
        $atts ['class']= $manual_class;
    }
    if (in_array('current-menu-item', $item->classes)) {
        $atts['class'] .= ' active';
    }
                // print_r($item);
                //
                /**
                * Filter the HTML attributes applied to a menu item's anchor element.
                *
                * @since 3.6.0
                * @since 4.1.0 The `$depth` parameter was added.
                *
                * @param array $atts {
                *     The HTML attributes applied to the menu item's `<a>` element, empty strings are ignored.
                    *
                    *     @type string $title  Title attribute.
                    *     @type string $target Target attribute.
                    *     @type string $rel    The rel attribute.
                    *     @type string $href   The href attribute.
                    * }
                    * @param object $item  The current menu item.
                    * @param array  $args  An array of {@see wp_nav_menu()} arguments.
                    * @param int    $depth Depth of menu item. Used for padding.
                    */
                $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );
                $attributes = '';
                foreach ( $atts as $attr => $value ) {
                    if ( ! empty( $value ) ) {
                        $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                        $attributes .= ' ' . $attr . '="' . $value . '"';
                    }
                }
                $item_output = $args->before;
                    // New
                    /*
                    if ($depth === 0 && in_array('menu-item-has-children', $classes)) {
                    $item_output .= '<a class="nav-link dropdown-toggle"' . $attributes .'data-toggle="dropdown">';
                        } elseif ($depth === 0) {
                        $item_output .= '<a class="nav-link"' . $attributes .'>';
                            } else {
                            $item_output .= '<a class="dropdown-item"' . $attributes .'>';
                                }
                                */
                                //
                                $item_output .= '<a'. $attributes .'>';
                                /** This filter is documented in wp-includes/post-template.php */
                                $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
                                $item_output .= '</a>';
                                $item_output .= $args->after;
                                /**
                                * Filter a menu item's starting output.
                                *
                                * The menu item's starting output only includes `$args->before`, the opening `<a>`,
                                * the menu item's title, the closing `</a>`, and `$args->after`. Currently, there is
                                * no filter for modifying the opening and closing `<li>` for a menu item.
                                    *
                                    * @since 3.0.0
                                    *
                                    * @param string $item_output The menu item's starting HTML output.
                                    * @param object $item        Menu item data object.
                                    * @param int    $depth       Depth of menu item. Used for padding.
                                    * @param array  $args        An array of {@see wp_nav_menu()} arguments.
                                    */
                                $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
                            }
                                    /**
                                    * Ends the element output, if needed.
                                    *
                                    * @see Walker::end_el()
                                    *
                                    * @since 3.0.0
                                    *
                                    * @param string $output Passed by reference. Used to append additional content.
                                    * @param object $item   Page data object. Not used.
                                    * @param int    $depth  Depth of page. Not Used.
                                    * @param array  $args   An array of arguments. @see wp_nav_menu()
                                    */
                                    public function end_el( &$output, $item, $depth = 0, $args = array() ) {
                                        if ($depth === 0) {
                                            $output .= "</li>\n";
                                        }
                                    }
                                }
                                function add_file_types_to_uploads($file_types){
                                    $new_filetypes = array();
                                    $new_filetypes['svg'] = 'image/svg+xml';
                                    $file_types = array_merge($file_types, $new_filetypes );
                                    return $file_types;
                                }
                                add_action('upload_mimes', 'add_file_types_to_uploads');
                                // Register WordPress nav menu
                                register_nav_menu('top', 'Top menu');
                                register_nav_menu('footer', 'Footer menu');
                                if( function_exists('acf_add_options_page') ) {
                                    acf_add_options_page();

                                }

                                function my_acf_google_map_api( $api ){

                                    $api['key'] = 'AIzaSyDElKd9kJ9hLcTX8gfCTAmg2UghCIsAZ3A';

                                    return $api;

                                }

                                add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

//language translate
                                if ( have_rows( 'tytuly_na_przyciskach', pll_current_language('slug') ) ) : while ( have_rows( 'tytuly_na_przyciskach', pll_current_language('slug') )  ) : the_row();
                                    $readmorebutton = get_sub_field( 'czytaj_wiecej' );
                                    $seealso = get_sub_field( 'zobacz_rowniez' );
                                    $literatura = get_sub_field( 'literatura' );
                                    $rozwin = get_sub_field( 'rozwin' );
                                    $download = get_sub_field( 'download' );
                                    $find = get_sub_field( 'find' );
                                    $ladowanie_mapy = get_sub_field( 'ladowanie_mapy' );
                                    $wyniki_wyszukiwania = get_sub_field( 'wyniki_wyszukiwania' );
                                    $imie_i_nazwisko = get_sub_field( 'imie_i_nazwisko' );
                                    $wojewodztwo = get_sub_field( 'wojewodztwo' );
                                    $wybierz_wojewodztwo = get_sub_field( 'wybierz_wojewodztwo' );
                                    $miasto = get_sub_field( 'miasto' );
                                    $kurs = get_sub_field( 'kurs' );
                                    $wyniki_ladowania = get_sub_field( 'wyniki_ladowania' );
                                    $placowka = get_sub_field( 'placowka' );
                                    $adres = get_sub_field( 'adres' );
                                    $zobacz_na_mapie = get_sub_field( 'zobacz_na_mapie' );
                                    $ukonczone_kursy = get_sub_field( 'ukonczony_kursy' );
                                    $poznaj_program = get_sub_field( 'poznaj_program' );
                                    $nic_nie_znalezlismy = get_sub_field( 'nic_nie_znalezlismy' );
                                    $przepraszamy_wyszukiwarka = get_sub_field( 'przepraszamy_wyszukiwarka' );
                                    $zapisz_sie = get_sub_field( 'zapisz_sie' );
                                    $sprawdz_szczegoly = get_sub_field( 'sprawdz_szczegoly' );
                                endwhile;  endif; 

                                //disable error
                                // Disable Emoji's

                                function disable_emojis() {
                                 remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
                                 remove_action( 'wp_print_styles', 'print_emoji_styles' );
                                 remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
                                 remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
                                 remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
                                 remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
                                 remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
                                 add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
                                 add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
                             }
                             add_action( 'init', 'disable_emojis' );

// Disable emoji in the TinyMCE WYSIWYG Editor 

                             function disable_emojis_tinymce( $plugins ) {
                                 if ( is_array( $plugins ) ) {
                                     return array_diff( $plugins, array( 'wpemoji' ) );
                                 } else {
                                     return array();
                                 }
                             }

// Disable DNS Prefetch

                             function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
                                 if ( 'dns-prefetch' == $relation_type ) {
                                     /** This filter is documented in wp-includes/formatting.php */
                                     $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

                                     $urls = array_diff( $urls, array( $emoji_svg_url ) );
                                 }

                                 return $urls;
                             }