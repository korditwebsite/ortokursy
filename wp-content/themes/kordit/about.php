<?php
/**
* Template Name: O nas
*/
?>
<?php get_header(); ?>
<main id="about">
	<section class="about-us">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 wow fadeInLeft">
					<h3><?php the_field("about_title"); ?></h3>
					<?php the_field("about_desc"); ?>
				</div>
				<div class="col-lg-6 wow fadeInRight">
					<?php echo wp_get_attachment_image( get_field('images'), "o-nas", "", array( "class" => "img-fluid", "alt" => "zdjecie" ) );  ?>
				</div>
			</div>
		</div>
	</section>
	<section class="team">
		<div class="container">
			<div class="row">
				<?php if( have_rows('team_wrapper') ):  while ( have_rows('team_wrapper') ) : the_row();  ?>
				<div class="col-lg-4 single-employee wow zoomIn">
					<?php echo wp_get_attachment_image( get_sub_field('img'), "malyprostokat", "", array( "class" => "img-fluid", "alt" => "zdjecie" ) );  ?>
					<h4><?php the_sub_field("name"); ?></h4>
					<?php the_sub_field("text"); ?>
				</div>
				<?php endwhile; else : endif; ?>
				<div class="col-lg-8 course-banner wow fadeInRight">
					
					<h4><?php the_field("title"); ?></h4>
					<div class="row">											<?php
						$args = array(
						'post_type'   => 'kursy',
						'post_status' => 'publish',
						'order' => 'DEC',
						'posts_per_page'=>'3',
						'tax_query' => [
						[
						'taxonomy' => 'popular',
						'field' => 'term_id',
						'terms' => 25,
						]
						],
						);
						$testimonials = new WP_Query( $args );
						if( $testimonials->have_posts() ) :
						?>
						<?php
						while( $testimonials->have_posts() ) :
							$testimonials->the_post();
						?>
						<div class="col-md-4">
							<div class="img-box">
															<?php
							if ( has_post_thumbnail()  ) {
								the_post_thumbnail( 'malyprostokat' );
							}
							?>
							</div>
							<h5><?php printf(get_the_title());  ?></h5>
							<?php the_content(); ?>
						</div>
						<?php endwhile; wp_reset_postdata(); ?>
					<?php else : esc_html_e( 'Kategoria w trakcie uzupełniania, zapraszamy wkrótce!', 'text-domain' ); endif; ?></div>
					<a href="<?php the_field("course_btn"); ?>"><?php the_field("course_btn_text"); ?></a>
					
				</div>
			</div>
		</div>
	</section>
</main>
<?php
get_template_part( 'parts/newsletter/newsletter');
?>
<?php get_footer(); ?>