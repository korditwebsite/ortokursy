<?php
/**
* Template Name: Instruktorzy
*/
?>
<?php get_header(); ?>
<main id="instructors">
	<section class="instructors">
		<?php if( have_rows('instructors_wrapper') ):  while ( have_rows('instructors_wrapper') ) : the_row();  ?>
		<div class="instructors-container">
			<div class="container">
				<div class="row">
					<div class="col-lg-5 wow fadeIn">
						<?php echo wp_get_attachment_image( get_sub_field('img'), "malyprostokat", "", array( "class" => "img-fluid", "alt" => "zdjecie" ) );  ?>
					</div>
					<div class="col-lg-7 wow fadeIn">
						<h3><?php the_sub_field("name"); ?></h3>
						<h4><?php the_sub_field("subtitle"); ?></h4>
						<?php the_sub_field("desc"); ?>
						<ul>
							<?php if( have_rows('instructors_course') ):  while ( have_rows('instructors_course') ) : the_row();  ?>
							<li><a href="<?php the_sub_field("course_link"); ?>"><?php the_sub_field("course"); ?></a></li>
							<?php endwhile; else : endif; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<?php endwhile; else : endif; ?>
	</section>
</main>
	<?php
	get_template_part( 'parts/question/question');
	?>
<?php get_footer(); ?>