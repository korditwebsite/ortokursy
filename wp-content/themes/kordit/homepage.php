<?php
/**
* Template Name: Strona główna
*/
?>
<?php get_header(); ?>
<main id="homepage">
	<section class="showcase">
		<?php if( have_rows('showcase_wrapper') ):  while ( have_rows('showcase_wrapper') ) : the_row();  ?>
			<div class="showcase-bg">
				<?php echo wp_get_attachment_image( get_sub_field('bg'), "full", "", array( "class" => "lazy" ) );  ?>
			</div>
			<div class="container showcase-wrapper wow fadeIn">
				<div class="row">
					<h1><?php the_sub_field("title"); ?></h1>
					<h2><?php the_sub_field("subtitle"); ?></h2>
					<ul>
						<?php if( have_rows('showcase_list') ):  while ( have_rows('showcase_list') ) : the_row();  ?>
							<li><?php the_sub_field("list_item"); ?></li>
						<?php endwhile; else : endif; ?>
					</ul>
					<a href="<?php the_sub_field("showcase_cta_link"); ?>"><?php the_sub_field("showcase_cta"); ?></a>
				</div>
			</div>
			<div class="showcase-banner wow fadeInUp">
				<div class="container">
					<div class="row">
						<div class="col-md-4">
							<h3><?php the_sub_field("banner_title"); ?></h3>
						</div>
						<div class="col-md-5">
							<?php the_sub_field("banner_text"); ?>
						</div>
						<div class="col-md-3">
							<a href="<?php the_sub_field("banner_btn_link"); ?>"><?php the_sub_field("banner_btn_text"); ?></a>
						</div>
					</div>
				</div>
			</div>
		<?php endwhile; else : endif; ?>
	</section>
	<section class="popular">
		<div class="container">
			<?php if( have_rows('popular') ):  while ( have_rows('popular') ) : the_row();  ?>
				<div class="row">
					<h2><?php the_sub_field("title"); ?></h2>
				</div>
				<div class="row">
					<?php if( have_rows('popular_item') ):  while ( have_rows('popular_item') ) : the_row();  ?>
						<div class="col-md-4 popular-box wow zoomIn">
							<?php echo wp_get_attachment_image( get_sub_field('img'), "malyprostokat", "", array( "class" => "img-fluid", "alt" => "logo" ) );  ?>
							<h5><?php the_sub_field("course_title"); ?></h5>
							<p><?php the_sub_field("desc"); ?></p>
							<a href="<?php the_sub_field("btn_link"); ?>" class="read-more"><?php the_sub_field("btn_text"); ?></a>
						</div>
					<?php endwhile; else : endif; ?>
				</div>
			<?php endwhile; else : endif; ?>
			<div class="row">
				<?php if( have_rows('popular_btn') ):  while ( have_rows('popular_btn') ) : the_row();  ?>
					<a href="<?php the_sub_field("popular_btn_link"); ?>" class="popular-btn"><?php the_sub_field("popular_btn_text"); ?></a>
				<?php endwhile; else : endif; ?>
			</div>
		</div>
	</section>
	<section class="main-link">
		<div class="container">
			<div class="row">
				<?php if( have_rows('link_box') ):  while ( have_rows('link_box') ) : the_row();  ?>
					<div class="col-lg-6 main-link-box wow fadeInLeft">
						<h3><?php the_sub_field("first_col_title"); ?></h3>
						<ul>
							<?php if( have_rows('first_col_list') ):  while ( have_rows('first_col_list') ) : the_row();  ?>
								<li><a href="<?php the_sub_field("first_col_link"); ?>"><?php the_sub_field("first_col_item"); ?></a></li>
							<?php endwhile; else : endif; ?>
						</ul>
					</div>
					<div class="col-lg-6 main-link-box wow fadeInRight">
						<h3><?php the_sub_field("second_col_title"); ?></h3>
						<ul>
							<?php if( have_rows('second_col_list') ):  while ( have_rows('second_col_list') ) : the_row();  ?>
								<li><a href="<?php the_sub_field("second_col_link"); ?>"><?php the_sub_field("second_col_item"); ?></a></li>
							<?php endwhile; else : endif; ?>
						</ul>
					</div>
				<?php endwhile; else : endif; ?>
			</div>
		</div>
	</section>
	<section class="image-box">
		<div class="container">
			<?php if( have_rows('image_box') ):  while ( have_rows('image_box') ) : the_row();  ?>
				<div class="row">
					<h2><?php the_sub_field("image_box_title"); ?></h2>
				</div>
				<div class="row justify-content-center">
					<?php if( have_rows('simple_box') ):  while ( have_rows('simple_box') ) : the_row();  ?>
						<div class="col-md-4 simple-box wow zoomIn">
							<?php echo wp_get_attachment_image( get_sub_field('images'), "o-nas", "", array( "class" => "img-fluid", "alt" => "zdjecie" ) );  ?>
							<h4><?php the_sub_field("simple_box_title"); ?></h4>
							<?php the_sub_field("simple_box_desc"); ?>
							<a href="<?php the_sub_field("simple_box_link"); ?>"><?php the_sub_field("simple_box_link_text"); ?></a>
						</div>
					<?php endwhile; else : endif; ?>
				</div>
			<?php endwhile; else : endif; ?>
		</div>
	</section>
	<section class="main-page-blog">
		<div class="container">
			<div class="row">
				<h2>Blog</h2>
			</div>
			<div class="row">
				<div class="owl-carousel owl-one">
					<?php get_template_part( 'templates/blog'); ?>
				</div>
			</div>
			<div class="row">
				<?php if( have_rows('news_btn') ):  while ( have_rows('news_btn') ) : the_row();  ?>
					<a href="<?php the_sub_field("news_btn_link"); ?>" class="news-btn"><?php the_sub_field("news_btn_text"); ?></a>
				<?php endwhile; else : endif; ?>
			</div>
		</div>
	</section>
</main>
<?php
get_template_part( 'parts/newsletter/newsletter');
?>
<?php get_footer(); ?>