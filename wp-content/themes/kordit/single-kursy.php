<?php
/**
* Template Name: Kursy
*/
?>
<?php get_header(2); ?>
<main id="courses-single">
	<section class="course-top">
		<div class="container">
			<div class="row">
				<div class="col-lg-3">
					<div class="course-top-img">
						<?php the_post_thumbnail( 'malyprostokat', "", array( "class" => "img-fluid", "alt" => "logo" ) ); ?>
						<img src="/wp-content/themes/kordit/img/line.png">
					</div>
				</div>
				<?php if( have_rows('course_main_info') ):  while ( have_rows('course_main_info') ) : the_row();  ?>
					<div class="col-lg-5">
						<p><?php the_sub_field("course_desc"); ?></p>
					</div>
					<div class="col-lg-4 course-top-banner">
						<div class="time">
							<img src="/wp-content/themes/kordit/img/date.png" alt="icon">
							<div class="text-box">
								<h5><?php the_sub_field("time_title"); ?></h5>
								<p><?php the_sub_field("time_answer"); ?></p>
							</div>
						</div>
						<div class="target">
							<img src="/wp-content/themes/kordit/img/man.png" alt="icon">
							<div class="text-box">
								<h5><?php the_sub_field("target_title"); ?></h5>
								<p><?php the_sub_field("target_answer"); ?></p>
							</div>
						</div>
						<div class="price">
							<img src="/wp-content/themes/kordit/img/price.png" alt="icon">
							<div class="text-box">
								<h5><?php the_sub_field("price_title"); ?></h5>
								<p><?php the_sub_field("price_answer"); ?></p>
							</div>
						</div>
						<a href="#course-info" class="sign_up-course"><?php the_sub_field("sign_up_text"); ?></a>
					</div>
				<?php endwhile; else : endif; ?>
			</div>
		</div>
	</section>
	<?php if( get_field('video') ): ?>
		<section class="video-box">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 embed-container">
						<?php the_field("video"); ?>
					</div>
				</div>
			</div>
		</section>
	<?php endif; ?>
	<section class="course-info" id="course-info">
		<div class="container">
			<div class="row">
				<div class="col-lg-7">
					<div class="syllabus" id="syllabus">
						<?php if( have_rows('syllabus') ):  while ( have_rows('syllabus') ) : the_row();  ?>
							<h4><?php the_sub_field("title"); ?></h4>
							<ul>
								<?php if( have_rows('syllabus_box') ):  while ( have_rows('syllabus_box') ) : the_row();  ?>
									<li><?php the_sub_field("item"); ?></li>
								<?php endwhile; else : endif; ?>
							</ul>
							<?php the_sub_field("text"); ?>
							<a href="<?php the_sub_field("download_link"); ?>" target="_blank" class="syllabus-download"><?php the_sub_field("download_btn_text"); ?></a>
						<?php endwhile; else : endif; ?>
					</div>
					<div class="term">
						<?php if( have_rows('term_wrapper') ): $licznik = 0; while ( have_rows('term_wrapper') ) : the_row();  ?>
							<h4><?php the_sub_field("term_title"); ?></h4>
							<?php if( have_rows('term_box') ):  while ( have_rows('term_box') ) : the_row();  ?>
								<div class="term-wrapper">
									<div class="term-box">
										<div class="term-title">
											<p class="place"><?php the_sub_field("group_place"); ?></p>
											<span class="slash">/</span>
											<p class="instructor"><?php the_sub_field("instructor"); ?>: <span class="instructor_name"><?php the_sub_field("instructor_name"); ?></span></p>
										</div>
										<div class="term-join">
											<div class="term-state">
												<p><?php the_sub_field("signup_title"); ?>: </p>
												<p><?php the_sub_field("signup_state"); ?></p>
											</div>
											<?php if( have_rows('week_simple') ):  while ( have_rows('week_simple') ) : the_row();  ?>
												<div class="term-week">
													<p class="week-nr"><?php the_sub_field("week_nr"); ?></p>
													<p><?php the_sub_field("week_value"); ?></p>
												</div>
											<?php endwhile; else : endif; ?>
										</div>
									</div>
									<?php if (!get_sub_field( 'dezaktywuj_grupe' ) == 1) { ?>
										<a href="#form-signup" group-title="<?php the_sub_field("group_place"); ?>" title="<?php wp_title('the_title_attribute();'); ?>" class="sign_up-btn sign_up-btn-title select-button-form sign_up-btn-title-<?php echo $licznik; ?>"><?php the_sub_field("sign_up-btn_text"); ?></a>
									<?php } else {
										echo "Zapisy zakończone";
									} ?>

								</div>
								<?php $licznik = $licznik + 1; endwhile; else : endif; 
							endwhile; else : endif; ?>
						</div>
					</div>
					<div class="col-lg"></div>
					<div class="col-lg-4">
						<div class="reviews-course">
							<?php if( have_rows('reviews_course_box') ):  while ( have_rows('reviews_course_box') ) : the_row();  ?>
								<h4><?php the_sub_field("title"); ?></h4>
								<div class="owl-carousel owl-two">
									<?php if( have_rows('reviews_course') ):  while ( have_rows('reviews_course') ) : the_row();  ?>
										<div class="rewiews-carousel">
											<p><?php the_sub_field("reviews"); ?></p>
											<p class="signature"><?php the_sub_field("signature"); ?></p>
										</div>
									<?php endwhile; else : endif; ?>
								</div>
							<?php endwhile; else : endif; ?>
						</div>
						<div class="also-check">
							<h4><?php the_field("title"); ?></h4>
							<?php
							$args = array(
								'post_type'   => 'kursy',
								'post_status' => 'publish',
								'order' => 'DEC',
								'posts_per_page'=>'3',
								'tax_query' => [
									[
										'taxonomy' => 'popular',
										'field' => 'term_id',
										'terms' => 25,
									]
								],
							);
							$testimonials = new WP_Query( $args );
							if( $testimonials->have_posts() ) :
								?>
								<?php
								while( $testimonials->have_posts() ) :
									$testimonials->the_post();
									?>
									<div class="also-check-wrapper">
										<?php
										if ( has_post_thumbnail()  ) {
											the_post_thumbnail( 'malyprostokat' );
										}
										?>
										<div class="also-check-box">
											<h5><?php printf(get_the_title());  ?></h5>
											<?php the_content(); ?>
											<a href="<?php echo get_permalink( $post->ID ); ?>" class=read-more><?php echo $readmorebutton; ?></a>
										</div>
									</div>
								<?php endwhile; wp_reset_postdata(); ?>
								<?php else : esc_html_e( 'Kategoria w trakcie uzupełniania, zapraszamy wkrótce!', 'text-domain' ); endif; ?>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="form-signup" id="form-signup">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<h3><?php echo $zapisz_sie; ?></h3>
						</div>
					</div>
					<div class="row">
						<div class="col-xl-12 col-form">
							<div class="inner-form-signup">
								<div role="form" class="wpcf7" id="wpcf7-f491-o1" lang="pl-PL" dir="ltr">
									<div class="screen-reader-response"></div>
									<form action="/kursy/maitland-poziom-2a/#wpcf7-f491-o1" method="post" class="wpcf7-form" novalidate="novalidate" data-count-fieldset="3">
										<fieldset class="fieldset-cf7mls cf7mls_current_fs" data-cf7mls-order="0"><div class="row">
											<div class="col-xl-12">
												<ul class="step-list">
													<li class="li-acti">1. Wybierz kurs</li>
													<li class="slash">/</li>
													<li>2. Uzupełnij dane</li>
													<li class="slash">/</li>
													<li>3. Podsumowanie</li>
												</ul>
											</div>
											<div class="col-xl-12">
												<h5>Wybierz kursy</h5>
											</div>
											<div class="col-xl-8">
												<p class="course-name">Nazwa kursu</p>
												<div class="select-wrapper">
													<span class="wpcf7-form-control-wrap menu-wszystkie">
														<select name="menu-wszystkie" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" id="selectset" aria-required="true" aria-invalid="false">
															<option><?php 
															the_title();
															?></option>
														</select>
													</span>
													<span class="wpcf7-form-control-wrap menu-crafta">
														<select id="group-select" name="menu-crafta" class="wpcf7-form-control wpcf7-select" aria-invalid="false">
															<?php if( have_rows('term_wrapper') ):  while ( have_rows('term_wrapper') ) : the_row();  ?>

																<?php if( have_rows('term_box') ):  while ( have_rows('term_box') ) : the_row();  ?>
																	<?php if (!the_sub_field( 'dezaktywuj_grupe' )): ?>
																		<?php if (!get_sub_field( 'dezaktywuj_grupe' ) == 1): ?>
																			<option>
																				<?php the_sub_field("group_place"); ?>
																			</option>
																		<?php endif ?>
																	<?php endif ?>
																<?php endwhile; else : endif; ?>
															<?php endwhile; else : endif; ?>
														</select>
													</span>
												</div>
											</div>
											<div class="col-xl-4">
												<p class="course-price">Do zapłaty:</p>
												<b><?php the_field( 'cena_kursu' ); ?> </b>
											</div>
										</div>
										<button type="button" class="cf7mls_next cf7mls_btn action-button" name="cf7mls_next">Dalej &gt;</button></fieldset><fieldset class="fieldset-cf7mls" data-cf7mls-order="1">
											<div class="row">
												<div class="col-xl-12">
													<ul class="step-list">
														<li>1. Wybierz kurs</li>
														<li class="slash">/</li>
														<li class="li-acti">2. Uzupełnij dane</li>
														<li class="slash">/</li>
														<li>3. Podsumowanie</li>
													</ul>
												</div>
												<div class="col-xl-12">
													<h5>Uzupełnij dane</h5>
												</div>
											</div>
											<div class="row">
												<div calss="col-xl-12">
													<div class="row">
														<div class="col-lg-4">
															<label>
																<span class="input-signup-title">Imię</span>
																<span class="wpcf7-form-control-wrap text-imie"><input type="text" name="text-imie" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Imie"></span>
															</label>
														</div>
														<div class="col-lg-4">
															<label>
																<span class="input-signup-title">Nazwisko</span>
																<span class="wpcf7-form-control-wrap text-nazwisko"><input type="text" name="text-nazwisko" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Nazwisko"></span>
															</label>
														</div>
														<div class="col-lg-4">
															<label>
																<span class="input-signup-title">Miasto</span>
																<span class="wpcf7-form-control-wrap text-miasto"><input type="text" name="text-miasto" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Miasto"></span>
															</label>
														</div>
														<div class="col-lg-4">
															<label>
																<span class="input-signup-title">Województwo</span>
																<span class="wpcf7-form-control-wrap text-wojewodztwo"><input type="text" name="text-wojewodztwo" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Województwo"></span>
															</label>
														</div>
														<div class="col-lg-4">
															<label>
																<span class="input-signup-title">Kod pocztowy</span>
																<span class="wpcf7-form-control-wrap text-kod-pocztowy"><input type="text" name="text-kod-pocztowy" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Kod pocztowy"></span>
															</label>
														</div>
														<div class="col-lg-4">
															<label>
																<span class="input-signup-title">Kraj</span>
																<span class="wpcf7-form-control-wrap text-kraj"><input type="text" name="text-kraj" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Kraj"></span>
															</label>
														</div>
														<div class="col-lg-4">
															<label>
																<span class="input-signup-title">Numer telefonu</span>
																<span class="wpcf7-form-control-wrap tel-telefon"><input type="text" name="tel-telefon" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Telefon"></span>
															</label>
														</div>
														<div class="col-lg-4">
															<label>
																<span class="input-signup-title">Email</span>
																<span class="wpcf7-form-control-wrap mail-mail"><input type="text" name="mail-mail" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Email"></span>
															</label>
														</div>
														<div class="col-lg-4 select-vat">
															<span class="input-signup-title">Czy potrzebujesz Fakturę VAT?*
																<span class="wpcf7-form-control-wrap menu-384"><select name="menu-384" class="wpcf7-form-control wpcf7-select" aria-invalid="false"><option value="Nie">Nie</option><option value="Tak">Tak</option></select></span></span>
															</div>
															<div data-id="group-vat" data-orig_id="group-vat" data-class="wpcf7cf_group" class="wpcf7cf-hidden">
																<div class="row">
																	<div class="col-lg-6"><label>NIP: <span class="wpcf7-form-control-wrap text-nip"><input type="text" name="text-nip" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false"></span></label></div>
																	<div class="col-lg-6"><label>Nazwa firmy: <span class="wpcf7-form-control-wrap text-nazwa-firmy"><input type="text" name="text-nazwa-firmy" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false"></span></label></div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<button type="button" class="cf7mls_back action-button" name="cf7mls_back">&lt; Powrót</button><button type="button" class="cf7mls_next cf7mls_btn action-button" name="cf7mls_next">Dalej &gt;</button></fieldset><fieldset class="fieldset-cf7mls" data-cf7mls-order="2">
													<div class="row ">
														<div class="col-xl-12">
															<ul class="step-list">
																<li>1. Wybierz kurs</li>
																<li class="slash">/</li>
																<li>2. Uzupełnij dane</li>
																<li class="slash">/</li>
																<li class="li-acti">3. Podsumowanie</li>
															</ul>
														</div>
														<div class="col-xl-12">
															<h5>Podsumowanie</h5>
														</div>
													</div>
													<div class="row last-step">
														<label>
															<span class="input-signup-title">Numer Prawa Wykonywania zawodu:*</span>
															<span class="wpcf7-form-control-wrap number-zawod"><input type="number" name="number-zawod" value="" class="wpcf7-form-control wpcf7-number wpcf7-validates-as-number" aria-invalid="false" placeholder="Nr prawa wykonywania zawodu"></span>
														</label>
														<label class="select-box">
															<span class="input-signup-title">Skąd dowiedziałeś się o kursie?</span>
															<span class="wpcf7-form-control-wrap menu-skad-wiesz"><select name="menu-skad-wiesz" class="wpcf7-form-control wpcf7-select" aria-invalid="false"><option value="z polecenia">z polecenia</option><option value="z Google">z Google</option><option value="z Facebooka">z Facebooka</option><option value="z reklamy w prasie">z reklamy w prasie</option><option value="z innego źródła">z innego źródła</option></select></span>
															<div data-id="group-tekst-skad-wiem" data-orig_id="group-tekst-skad-wiem" data-class="wpcf7cf_group" class="wpcf7cf-hidden">
																<span class="wpcf7-form-control-wrap text-inne-zrodlo"><input type="text" name="text-inne-zrodlo" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false"></span>
															</div>
														</label>
														<label>
															<span class="wpcf7-form-control-wrap acceptance-497"><span class="wpcf7-form-control wpcf7-acceptance"><span class="wpcf7-list-item"><label><input type="checkbox" name="acceptance-497" value="1" aria-invalid="false"><span class="wpcf7-list-item-label">Administratorem Państwa danych osobowych jest OrtoKursy sp. Z o.o. z siedzibą w Warszawie przy ul. Ostzyckiej 2/4. Podane dane osobowe będą przetwarzane w celu realizacji zamówienia oraz dla wypełnienia prawnie usprawiedliwionych celów realizowanych przez administratora danych (marketing bezpośredni swoich towarów i usług). W przypadku wyrażenia zgody na przesyłanie informacji handlowych drogą mailową/zgody na kontakt telefoniczny w celach marketingu bezpośredniego dane osobowe będą przetwarzane także w tym celu. Przysługuje Pani/Panu prawo dostępu do treści swoich danych oraz ich poprawiania. Osoba, której dane dotyczą, ma prawo w dowolnym momencie wycofać zgodę (wysyłając żądanie na adres e-mail: info@ortokursy.pl). Wycofanie zgody nie wpływa na zgodność z prawem przetwarzania, którego dokonano na podstawie zgody przed jej wycofaniem.</span></label></span></span></span>
														</label>
														<label>
															Akceptacja warunków regulaminu*
															<span class="wpcf7-form-control-wrap acceptance-regulamin"><span class="wpcf7-form-control wpcf7-acceptance"><span class="wpcf7-list-item"><label><input type="checkbox" name="acceptance-regulamin" value="1" aria-invalid="false"><span class="wpcf7-list-item-label">Akceptuję warunki regulaminu.</span></label></span></span></span>
															<span class="wpcf7-form-control-wrap acceptance-mail"><span class="wpcf7-form-control wpcf7-acceptance"><span class="wpcf7-list-item"><label><input type="checkbox" name="acceptance-mail" value="1" aria-invalid="false"><span class="wpcf7-list-item-label">Wyrażam zgodę na otrzymywanie informacji handlowych na podany przeze mnie adres e-mail , wysyłanych przez Ortokursy lub naszych partnerów.</span></label></span></span></span>
															<small>* Potrzebujemy Twojej zgody, aby móc przesyłać dodatkowe informacje na temat kursu. Możesz zawsze zrezygnować z otrzymywania od nas informacji.</small>
														</label>

														<input type="submit" value="Wyślij" class="wpcf7-form-control wpcf7-submit" disabled=""><span class="ajax-loader"></span>
													</div><input type="button" value="< Powrót" class="cf7mls_back action-button" name="cf7mls_back"></fieldset><div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
												</div>
											</div>
										</div>
									</div>
								</section>
							</main>
							<?php
							get_template_part( 'parts/question/question');
							?>
							<?php get_footer(); ?>
							<script>
								var parts = window.location.href.split('/');
								var lastSegment = parts.pop() || parts.pop();

								$("#selectset").val(lastSegment);
							</script>