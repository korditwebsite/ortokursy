<?php
/**
* Template Name: Kursy
*/
?>
<?php get_header(); ?>
<main id="course-types">
  <div class="container">
    <div class="row">



      <?php
      $taxonomy = 'types';
      $terms = get_terms($taxonomy, $args = array(
        'hide_empty' => true,
          'meta_key' => 'acf_course_order',
        'orderby' => 'meta_value',
        'order' => 'DESC'
      ));
      ?>
      <?php foreach( $terms as $term ) :
        $catIMG = get_field('category_image', 'jobpost_category_' . $term->term_id);
        ?>
        <div class="col-md-4 course-types-box wow zoomIn">
          <?php echo get_term_thumbnail( $term->term_taxonomy_id, $size = 'category-thumb', $attr = '' ) ?>
          <h3><?php echo $term->name; ?></h3>
          <p><?php echo $term->description; ?></p>
          <a class="image-nav-block" id="post-<?php the_ID(); ?>" href="<?php echo get_term_link($term->slug, $taxonomy); ?>"><?php echo $readmorebutton; ?>
        </a>
      </div>
    <?php endforeach;?>
  </div>
</div>
</main>
<?php
get_template_part( 'parts/question/question');
?>
<?php get_footer(); ?>