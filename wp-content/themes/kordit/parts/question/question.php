<section class="question">
  <div class="container question-wrapper">
    <div class="row wow zoomIn">
      <?php if( have_rows('question_wrapper', pll_current_language('slug')) ):  while ( have_rows('question_wrapper', pll_current_language('slug')) ) : the_row();  ?>
        <div class="col-lg-12 ">
          <h4><?php the_sub_field("question_title", pll_current_language('slug')); ?></h4>
          <?php the_sub_field("question_desc", pll_current_language('slug')); ?>
          <ul>
            <li><a href="mailto:<?php the_sub_field("question_mail", pll_current_language('slug')); ?>"><img src="/wp-content/themes/kordit/img/cta-mail.png"><?php the_sub_field("question_mail", pll_current_language('slug')); ?></a></li>
            <li><img src="/wp-content/themes/kordit/img/cta-call.png"><a href="tel:+48<?php the_sub_field("question_call", pll_current_language('slug')); ?>"><?php the_sub_field("question_call_text", pll_current_language('slug')); ?></a></li>
          </ul>
        </div>
      <?php endwhile; else : endif; ?>
    </div>
  </div>
</section>