<section class="newsletter">
	<div class="container">
		<div class="row wow zoomIn">
			<?php if( have_rows('newsletter_wrapper', pll_current_language('slug')) ):  while ( have_rows('newsletter_wrapper', pll_current_language('slug')) ) : the_row();  ?>
				<div class="col-lg-6 newsletter-box-1">
					<h4><?php the_sub_field("newsletter_title1", pll_current_language('slug')); ?></h4>
					<?php the_sub_field("newsletter_desc1", pll_current_language('slug')); ?>
					<a href="<?php the_sub_field("newsletter_btn", pll_current_language('slug')); ?>"><?php the_sub_field("newsletter_btn_text", pll_current_language('slug')); ?></a>
				</div>
				<div class="col-lg-6 newsletter-box-2">
					<h4><?php the_sub_field("newsletter_title2", pll_current_language('slug')); ?></h4>
					<?php the_sub_field("newsletter_desc2", pll_current_language('slug')); ?>
					<?php echo do_shortcode('[contact-form-7 id="132" title="Formularz 2"]'); ?>
				</div>
			<?php endwhile; else : endif; ?>
		</div>
	</div>
</section>