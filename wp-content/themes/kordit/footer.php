<footer>
	<div class="container">
		<div class="row first-row">
			<div class="col-lg-4 footer-box">
				<h5><?php the_field('footer_about_title',pll_current_language('slug')); ?></h5>
				<?php the_field('footer_about_desc', pll_current_language('slug')); ?>
			</div>
			<div class="col-lg-5 footer-box footer-box-courses">
				<h5><?php the_field('footer_course_title', pll_current_language('slug')); ?></h5>
				<?php if (is_page_template( 'homepage.php' ) ) {
					$liczbapostow = 3;
				} else {
					$liczbapostow = -1;
				}
				$nazwaKategori = "kursy";
				$args = array(
					'post_type'   => 'kursy',
					'post_status' => 'publish',
					'order' => 'DESC',
					'posts_per_page'=> 30
				);
				$courses = new WP_Query( $args );
				if( $courses->have_posts() ) :
					?>
					<ul>
						<?php
						while( $courses->have_posts() ) :
							$courses->the_post();
							?>

							<li>
								<a href="<?php the_permalink(); ?>" class="read-more"><?php the_title(); ?></a>
							</li>


						<?php endwhile; wp_reset_postdata(); else : esc_html_e( 'Kategoria w trakcie uzupełniania, zapraszamy wkrótce!', 'text-domain' ); endif; ?>
					</ul>
				</div>
				<div class="col-lg-3 footer-box">
					<h5><?php the_field('footer_menu_title', pll_current_language('slug')); ?></h5>
					<div class="menu-footer-menu-container">
						<?php
						wp_nav_menu([
							'menu'            => 'footer',
							'theme_location'  => 'footer',
							'container'       => 'div',
							'container_class' => 'nawigacja-menu',
							'menu_class'      => 'navbar-nav',
							'depth'           => 2,
							'fallback_cb'     => 'bs4navwalker::fallback',
							'walker'          => new bs4navwalker(),
						]);
						?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 footer-box">
					<h5><?php the_field('footer_contact_title', pll_current_language('slug')); ?></h5>
					<?php the_field('footer_contact_desc', pll_current_language('slug')); ?>
				</div>
				<div class="col-lg-3 footer-box footer-social">
					<h5><?php the_field('footer_social_title', pll_current_language('slug')); ?></h5>
					<ul>
						<li><a href="<?php the_field('fb_link', pll_current_language('slug')); ?>"><img src="/wp-content/themes/kordit/img/fb.png" alt="icon-social"></a></li>
						<li><a href="<?php the_field('in_link', pll_current_language('slug')); ?>"><img src="/wp-content/themes/kordit/img/in.png" alt="icon-social"></a></li>
						<li><a href="<?php the_field('yt_link', pll_current_language('slug')); ?>"><img src="/wp-content/themes/kordit/img/youtube.png" alt="icon-social"></a></li>
					</ul>
				</div>
				<div class="col-lg-6 footer-box">
					<div class="fb-page" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
					
					<div id="face-box" class="news-fb"><div class="fb-page" data-href="https://www.facebook.com/ortokursy/?ref=br_rs" data-tabs="timeline" data-small-header="false" data-height="130" data-width="555" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="<?php the_field('facebook-link'); ?>" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/ortokursy/?ref=br_rs"></a></blockquote>
					</div>
				</div>
			</div>
		</div>
		<div class="copyright">
			<p>&copy; OrtoKursy Sp. z.o.o. 2019. Wszelkie prawa zastrzeżone</p>
			<p>Wdrożenie: <a href="https://www.combomarketing.pl/" target="_blank">Combo Marketing</a></p>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = 'https://connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v3.1&appId=2001495613512113&autoLogAppEvents=1';
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAz9ta0NVh_Eyb689pz0dB6EUrB7U3LSmo&callback=initMap" async defer></script>
</body>
</html>