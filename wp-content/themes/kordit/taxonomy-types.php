<?php
/**
* Template Name: Kursy grupy
*/
?>
<?php get_header(); ?>
<main id="types">
     <section class="level-title">
          <div class="container">
               <div class="row">
                    <div class="col-md-4">
                         <div class="level-title-img">
                              <?php the_post_thumbnail( 'malyprostokat', "", array( "class" => "img-fluid", "alt" => "logo" ) ); ?>
                              <img src="/wp-content/themes/kordit/img/line.png">
                         </div>
                    </div>
                    <div class="col-md-8">
                         <p><?php
                         $term = get_queried_object();
                         $desc = get_field('desc', $term); ?>
                         <?php echo $desc; ?></p>
                    </div>
               </div>
          </div>
     </section>
     <section class="levels">
          <div class="container">
               <div class="row">
                    <?php
                    $case_study_cat_slug = get_queried_object()->slug;
                    ?>
                    <?php
                    $al_tax_post_args = array(
                    'post_type' => 'kursy', // Your Post type Name that You Registered
                    'posts_per_page' => 999,
                    'order' => 'ASC',
                    'tax_query' => array(
                         array(
                              'taxonomy' => 'types',
                              'field' => 'slug',
                              'terms' => $case_study_cat_slug
                         )
                    )
               );
                    $al_tax_post_qry = new WP_Query($al_tax_post_args);
                    if($al_tax_post_qry->have_posts()) :
                         while($al_tax_post_qry->have_posts()) :
                              $al_tax_post_qry->the_post();
                              echo '<div class="post-excerpt col-md-6">';
                              ?>
                              <div class="level-box">
                                   <?php the_post_thumbnail( 'malyprostokat', "", array( "class" => "img-fluid", "alt" => "logo" ) ); ?>
                                   <h3><?php the_title(); ?></h3>
                              </div>
                              <?php the_content(); ?>
                              <a href="<?php echo get_permalink( $post->ID ); ?>" class=read-more><?php echo $sprawdz_szczegoly; ?></a>
                         </div>
                         <?php
                    endwhile;
               endif;
               ?>
          </div>
     </div>
</section>
<section class="related">
     <div class="container">
          <div class="row">
               <?php
               $case_study_cat_name = get_queried_object()->name;
               $term = get_queried_object();
               ?>
               <h2><?php echo $case_study_cat_name; ?> - Kursy tematyczne</h2>
               <h1><?php //the_field( 'thematic_courses', $term ); ?></h1>
          </div>
          <?php 

          $posts = get_field( 'thematic_courses', $term );

          if( $posts ): ?>
               <div class="row related-wrapper">
                    <?php foreach( $posts as $p ):  ?>
                        <div class="col-xl-4 col-md-4 col-12 related-box">
                         <h4><?php echo get_the_title( $p->ID ); ?></h4>
                         <p><?php the_field("descr",  $p->ID ); ?></p>
                         <a href="<?php echo get_permalink( $p->ID ); ?>">
                              Sprawdź szczegóły
                         </a>

                    </div>
               <?php endforeach; ?>
          </div>
     <?php endif; ?>
</div>

</section>

</main>
<?php
get_template_part( 'parts/question/question');
?>
<?php get_footer(); ?>