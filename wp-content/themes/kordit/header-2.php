<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<?php wp_head(); ?>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title><?php wp_title('the_title_attribute();'); ?></title>
</head>
<body <?php body_class( 'class-name' ); ?>>
	<header>
		<div class="top-header">
			<div class="container top-header-wrapper">
				<div class="header-search">
					<?php get_search_form(); ?>
				</div>
				<div class="header-phone">
					<a href="tel:+48<?php the_field('header_call', pll_current_language('slug')); ?>">Tel.: +48 <?php the_field('header_call_text', pll_current_language('slug')); ?></a>
				</div>
				<div class="header-social">
					<ul>
						<li><a href="<?php the_field('fb_link', pll_current_language('slug')); ?>"><img src="/wp-content/themes/kordit/img/fb.png" alt="icon-social"></a></li>
						<li><a href="<?php the_field('in_link', pll_current_language('slug')); ?>"><img src="/wp-content/themes/kordit/img/in.png" alt="icon-social"></a></li>
						<li><a href="<?php the_field('yt_link', pll_current_language('slug')); ?>"><img src="/wp-content/themes/kordit/img/youtube.png" alt="icon-social"></a></li>
					</ul>
				</div>
			</div>
		</div>
		<nav class="navbar navbar-expand-lg navbar-dark scrolling-navbar">
			<div class="container">
				<a class="navbar-brand" href="/">
					<div class="brand-logo">
						<?php $custom_logo_id = get_theme_mod( 'custom_logo' );
						$logo = wp_get_attachment_image_src( $custom_logo_id , 'headerlogo' );
						if ( has_custom_logo() ) {
							echo '<img class="img-fluid" src="'. esc_url( $logo[0] ) .'">';
						} else {
							echo '<h1>'. get_bloginfo( 'name' ) .'</h1>';
						} ?>
					</div>
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7" aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent-7">
					<?php
					wp_nav_menu([
						'menu'            => 'top',
						'theme_location'  => 'top',
						'container'       => 'div',
						'container_class' => '',
						'menu_id'         => false,
						'menu_class'      => 'navbar-nav',
						'depth'           => 2,
						'fallback_cb'     => 'bs4navwalker::fallback',
						'walker'          => new bs4navwalker()
					]);
					?>
				</div>
			</div>
		</nav>
	</header>
	<?php if ( !is_page_template( 'homepage.php' ) ): ?>
		<div class="header-bg-2">
			<div class="container">
				<div class="header-present">
					<h2><?php the_title(); ?></h2>
					<h4><?php the_field("subtitle"); ?></h4>
					<?php if(ICL_LANGUAGE_CODE=='en'): ?>
						<a href="#course-info" class="header-link">Get to know the program</a>
						<?php elseif(ICL_LANGUAGE_CODE=='pl'): ?>
							<a href="#course-info" class="header-link">Poznaj program</a>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<?php endif ?>