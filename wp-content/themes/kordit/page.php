<?php
/**
* Template Name: Podstrona
*/
?>
<?php get_header(); ?>
<main id="page">
	<div class="container">
		<div class="row">
			<div class="col-lg-9">
				<?php the_content(); ?>
			</div>
			<div class="col-lg-3">
				<div class="contact-sidebar">
					<h4><?php the_field("sidebar_title"); ?></h4>
					<?php the_field("sidebar_desc"); ?>
					<ul>
						<li><img src="/wp-content/themes/kordit/img/cta-mail.png"><a href="mailto:<?php the_field("sidebar_mail"); ?>"></a><?php the_field("sidebar_mail"); ?></li>
						<li><img src="/wp-content/themes/kordit/img/cta-call.png"><a href="tel:+48<?php the_field("sidebar_call"); ?>"></a>+48 <?php the_field("sidebar_call_text"); ?></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</main>
<?php get_footer(); ?>