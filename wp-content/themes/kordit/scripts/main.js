var $ = jQuery;
//Nawigacja 
$("#menu-button").click(function(){
	$("nav").toggleClass("active");
});
$("a").click(function(){
	$("nav").removeClass("active");
	$("#menu-button").removeClass("open");
});

$('#face-slider').hover(
    function(){ $('#face-slider').stop().animate({"right": "0"}, 1000); },
    function(){ $('#face-slider').stop().animate({"right": "-290px"}, 1000); }
    );
//Nawigacja header 1
$(".menu-button-1").click(function(){
	$("main").toggleClass("blur");
});
$("a").click(function(){
	$("main").removeClass("blur");
});

//Nawigacja header 2
$(".menu-button-2").click(function(){
	$("main").toggleClass("blur-1");
});
$("a").click(function(){
	$("main").removeClass("blur-1");
});

$(document).ready(function(){
//Owl carousel
$('.owl-one').owlCarousel({
    loop:true,
    margin:20,
    responsiveClass:true,
    autoplay:true,
    autoplayTimeout:3000,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:false
        },
        1000:{
            items:2,
            nav:true,
            loop:false
        }
    }
});
$('.owl-two').owlCarousel({
    loop:true,
    margin:20,
    responsiveClass:true,
    autoplay:true,
    nav: true,
    navText: ["Poprzednia opinia", "Następna opinia"],
    autoplayTimeout:10000,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:false
        },
        1000:{
            items:1,
            nav:true,
            loop:false
        }
    }
});

});



function openCity(evt, cityName) {

  // Declare all variables
  var i, tabcontent, tablinks;

  // Get all elements with class="tabcontent" and hide them
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
}

  // Get all elements with class="tablinks" and remove the class "active"
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
}

  // Show the current tab, and add an "active" class to the button that opened the tab
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

$(document).ready(function() {
  $('#show-hidden-btn').click(function() {
    $('.hidden-text').slideToggle("slow");
});
});  

$(document).ready(function() {
	
//usuwanie przycisków, gdy content pusty. Pomaga przy sliderze
$('button').filter(function() {
	return $(this).html().trim().length == 0
}).remove();

//aktywacja nawigacji
$('#menu-button button').on('click', function() {
	$(this).toggleClass('isActive');
});
});
window.addEventListener('DOMContentLoaded',function(){
	new SmartPhoto(".js-smartPhoto");
});


var iteracja = 0; 

$( document ).ready(function() {


	var wow = new WOW(
	{
    boxClass:     'wowparalax',      // animated element css class (default is wow)
    animateClass: 'animated', // animation css class (default is animated)
    offset:       0,          // distance to the element when triggering the animation (default is 0)
    mobile:       true,       // trigger animations on mobile devices (default is true)
    live:         true,       // act on asynchronously loaded content (default is true)
    callback:     function(box) {
    	do {
    		var svganimacja = "#paralax-svg-" + iteracja;
    		var svg = new Walkway({
    			selector: svganimacja + ' svg',
    			easing: 'easeInOutCubic',
    			duration: 5100
    		}).draw();
    		iteracja++;
    	}
    	while (iteracja < 10);
    },
    scrollContainer: null // optional scroll container selector, otherwise use window
}
);
	wow.init();
});
new WOW().init();


$(window).on('scroll', function (event) {
    var scrollValue = $(window).scrollTop();
    if (scrollValue > 120) {
        $('.navbar').addClass('affix');
    } else{
        $('.navbar').removeClass('affix');
    }
});

$('.look-more').click(function(){
    $(".inner-container-map h5").addClass("active");
    setTimeout(
      function() 
      {
        $(".inner-container-map h5").removeClass("active");
    }, 3000);
});

//document.getElementById("defaultOpen").click();

//Ustawianie odpowiedniego selecta w single kursy
var str = $( "title" ).text();
var text = str.substr(0, str.length-12); 
var groupnumber = $(this).attr('group-title');
var ide = $('.select-button-form').attr('group-title');

$('.sign_up-btn-title').ready(function(){ 
    $('#selectset').val(text);
})
$('.sign_up-btn-title').click(function(){ 
    $('#selectset').val(text);
})

$('.sign_up-btn-title').click(function(){
    var groupnumber = $(this).attr('group-title');
    $('#group-select').val(groupnumber);
})