<?php
/**
* Template Name: Zniżka
*/
?>
<?php get_header(); ?>
<main id="discount">
	<section class="discount-wrapper">
		<div class="container">
			<div class="row">
				<?php if( have_rows('discount_wrapper') ):  while ( have_rows('discount_wrapper') ) : the_row();  ?>
				<div class="col-lg-6 wow fadeInLeft">
					<h4><?php the_sub_field("discount_header"); ?></h4>
					<?php the_sub_field("desc"); ?>
					<h3><?php the_sub_field("discount_list_title"); ?></h3>
					<ul class="discount-list">
						<?php if( have_rows('discount_list') ):  while ( have_rows('discount_list') ) : the_row();  ?>
						<li><p class="discount-list-item"><?php the_sub_field("discount_list_item"); ?></p><p><?php the_sub_field("discount_list_value"); ?></p></li>
						<?php endwhile; else : endif; ?>
					</ul>
				</div>
				<div class="col-lg-6 wow fadeInRight">
					<?php  echo wp_get_attachment_image( get_sub_field('img'), "o-nas" ); ?>
					<div class="discount-info">
						<h4><?php the_sub_field("discount_info_title"); ?></h4>
						<div class="discount-info-wrapper">
							<ul class="discount-info-box">
								<?php if( have_rows('discount_info_list') ):  while ( have_rows('discount_info_list') ) : the_row();  ?>
									<li><?php the_sub_field("discount_info_item"); ?></li>
								<?php endwhile; else : endif; ?>
							</ul>
						</div>
					</div>
				</div>
				<?php endwhile; else : endif; ?>
			</div>
		</div>
	</section>
</main>
	<?php
	get_template_part( 'parts/question/question');
	?>
<?php get_footer(); ?>