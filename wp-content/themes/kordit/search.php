<?php
get_header();
?>
<div class="container found-box">
  <?php
  $s=get_search_query();
  $args = array(
    's' =>$s
  );
  // The Query
  $the_query = new WP_Query( $args );
  if ( $the_query->have_posts() ) {
    _e("<h2 style='font-weight:bold;color:#000'>Znaleziono: ".get_query_var('s')."</h2>");
    while ( $the_query->have_posts() ) {
      $the_query->the_post();
      ?>
      <li class="found">
        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
      </li>
      <?php
    }
  }else{
    ?>
    <h2 style='font-weight:bold;color:#000'><?php echo $nic_nie_znalezlismy; ?></h2>
    <div class="alert alert-info">
      <p><?php echo $przepraszamy_wyszukiwarka; ?></p>
    </div>
  <?php } ?>
</div>
<?php
get_footer();
?>