<div class="literature">
	<div class="row">
		<div class="col-lg-12 literature-box">
			<p class="literature-title"><?php echo $literatura; ?>:</p><div id="show-hidden-btn"><?php echo $rozwin; ?></div>
		</div>
	</div>
	<div class="hidden-text" style="display: none;"> 
		<?php the_sub_field("literature"); ?>
	</div>
</div>