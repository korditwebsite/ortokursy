<div class="row comparison">
	<?php if( have_rows('comparison') ):  while ( have_rows('comparison') ) : the_row();  ?>
	<div class="col-md-6 comparison-box">
		<img src="/wp-content/themes/kordit/img/add.png" alt="sign icon">
		<ul>
			<?php if( have_rows('pros') ):  while ( have_rows('pros') ) : the_row();  ?>
			<li><?php the_sub_field("simple_pros"); ?></li>
			<?php endwhile; else : endif; ?>
		</ul>
	</div>
	<div class="col-md-6 comparison-box">
		<img src="/wp-content/themes/kordit/img/substract.png" alt="sign icon">
		<?php if( have_rows('minus') ):  while ( have_rows('minus') ) : the_row();  ?>
		<li><?php the_sub_field("simple_minus"); ?></li>
		<?php endwhile; else : endif; ?>
	</div>
	<?php endwhile; else : endif; ?>
</div>