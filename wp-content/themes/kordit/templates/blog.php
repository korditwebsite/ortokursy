<?php 
$inne = get_field_object('field_5d5c09dab0334', pll_current_language('slug') );
?>

<?php 
$args = array(
	'posts_per_page'    => 3,
'post_type'     => 'post',  //choose post type here
'order' => 'DESC',
); 
// The Query
$query = new WP_Query( $args );
// The Loop
if ( $query->have_posts() ) {
	while ( $query->have_posts() ) {
		$query->the_post();
		?>
		<?php
		if (get_the_post_thumbnail_url($post)) {
			$grafika = get_the_post_thumbnail_url($post);
		} else {
			$grafika ="/wp-content/uploads/2019/10/post.jpg";
		}
		?> 
		<!-- article -->
		<div class="simple-post">
			<div class="post-loop">
				<img src="<?php echo $grafika; ?>">
			</div>
			<div class="row news-title post-carousel">
				<div class="col-lg-12">
					<h5><? echo $post->post_title; ?></h5>
					<p class="date"><? echo date('d.m', strtotime($post->post_date)); ?> <? echo date('Y', strtotime($post->post_date)); ?></p>
					<p class="max-word-height">
						<?echo wp_trim_words( $post->post_content,35,"…" ); ?>
					</p>
					<?php if(ICL_LANGUAGE_CODE=='en'): ?>
						<a class="read-more" style="width: 100%;" href="<?php echo get_permalink($post,false) ?>">Read more</a>
						<?php elseif(ICL_LANGUAGE_CODE=='pl'): ?>
							<a class="read-more" style="width: 100%;" href="<?php echo get_permalink($post,false) ?>">Czytaj więcej</a>
						<?php endif; ?>

					</div>
				</div>
			</div>
			<!-- /article -->
			<?php
		}
	} else {
// no posts found
	}
// Restore original Post Data 
	wp_reset_postdata();