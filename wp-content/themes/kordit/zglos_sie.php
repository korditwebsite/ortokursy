<?php
/**
* Template Name: Zgłoś się
*/

get_header(); 

$current_lang = pll_current_language("slug");
if ($current_lang !== 'pl') {
    if (wp_redirect(home_url())) {
        exit;
    }
}
?>
<main id="apply-to">
    <section class="top-apply-to">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-md-6 col-12">
                    <h5><?php the_field( 'tytul' ); ?></h5>
                </div>
                <div class="col-xl-6 offet-xl-1 col-md-6 col-12">
                    <p><?php the_field( 'podpis' ); ?></p>
                </div>
            </div>
        </div>
    </section>
    <?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['zglos_submit'])) {
            $filename = $_FILES['zglos_image']['name'];
            $wp_filetype = wp_check_filetype( basename($filename), null );
            $wp_upload_dir = wp_upload_dir();
            move_uploaded_file( $_FILES['zglos_image']['tmp_name'], $wp_upload_dir['path']  . '/' . $filename );
            $attachment = array(
                'guid' => $wp_upload_dir['url'] . '/' . basename( $filename ), 
                'post_mime_type' => $wp_filetype['type'],
                'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
                'post_content' => '',
                'post_status' => 'inherit'
            );
            $filename = $wp_upload_dir['path']  . '/' . $filename;
            $attach_id = wp_insert_attachment($attachment, $filename, 37);
            require_once( ABSPATH . 'wp-admin/includes/image.php' );
            $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
            wp_update_attachment_metadata( $attach_id, $attach_data );
            
            $new_post_title = $_POST['zglos_name'];
            $new_post_address = $_POST['zglos_address'];
            $new_post_category = $_POST['zglos_voivodeship'];
            $new_post_place = $_POST['zglos_place'];
            $new_post_phone = $_POST['zglos_phone'];
            $new_post_email = $_POST['zglos_email'];
            
            if(isset($_POST['zglos_kurs'])) {
                $new_post_courses = $_POST['zglos_kurs'];
                foreach ($new_post_courses as $new_post_course){
                    $acf_post_course = $new_post_course . ", " . $acf_post_course;
                }}
            else {
                $acf_post_course = "Brak kursów";
            }
            
            $new_post = array(
                'post_title'    => $new_post_title,
                'post_status'   => "draft",
                'post_author'   => 1,
                'post_type'     => "fizjoterapeuci",
                'tax_input'    => array(
                    "wojewodztwo" => $new_post_category
                ),
            );

            $new_post_id = wp_insert_post($new_post);
            
            update_field('field_5d696d6e42e9d', $new_post_address, $new_post_id);
            update_field('field_5d30801452a45', $new_post_address, $new_post_id);
            update_field('field_5d696cb5b6672', $new_post_place, $new_post_id);
            update_field('field_5d696d8942e9e', $new_post_phone, $new_post_id);
            update_field('field_5d696d9542e9f', $new_post_email, $new_post_id);
            update_field('field_5d317d171abe8', $acf_post_course, $new_post_id);
            
            set_post_thumbnail($new_post_id, $attach_id);
        }
    }
    ?>
    <section class="apply-to-form">
        <div class="container">
            <?php 
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                if (isset($_POST['zglos_submit'])) {
                    echo '<div class="alert alert-success mt-4 mb-5"> Twoje zgłoszenie zostało wysłane pomyślnie!</div>';
                }
            }
            ?>
            <form method="POST" action="" enctype="multipart/form-data">
            <div class="row">
                <div class="col-xl-4 col-md-4 col-6">
                    <label>
                        Imię i nazwisko fizjoterapeuty
                        <input type="text" class="form-control" id="zglos_name" name="zglos_name" required>
                    </label>
                </div>
                <div class="col-xl-4 col-md-4 col-6">
                    <label>
                        Województwo
                        <select name="zglos_voivodeship" id="zglos_voivodeship" class="form-control">
                            <option selected="selected disabled">Wybierz wojewodztwo</option>
                            <option value="45">Dolnośląskie</option>
                            <option value="44">Kujawsko-Pomorskie</option>
                            <option value="43">Lubelskie</option>
                            <option value="42">Lubuskie</option>
                            <option value="41">Łódzkie</option>
                            <option value="40">Małopolskie</option>
                            <option value="39"> Mazowieckie</option>
                            <option value="38">Opolskie</option>
                            <option value="37">Podkarpackie</option>
                            <option value="36">Podlaskie</option>
                            <option value="35">Pomorskie</option>
                            <option value="34">Śląskie</option>
                            <option value="33">Świętokrzyskie</option>
                            <option value="32">Warmińsko-Mazurskie</option>
                            <option value="31">Wielkopolskie</option>
                            <option value="30">Zachodnio-Pomorskie</option>
                        </select>
                    </label>
                </div>
                <div class="col-xl-4 col-md-4 col-6">
                    <label>
                        Miasto
                        <input type="text" class="form-control" id="zglos_city" name="zglos_city" required>
                    </label>
                </div>
                <div class="col-xl-4 col-md-4 col-6">
                    <label>
                        Nazwa placówki
                        <input type="text" class="form-control" id="zglos_place" name="zglos_place" required>
                    </label>
                </div>
                <div class="col-xl-4 col-md-4 col-6">
                    <label>
                        Adres placówki
                        <input type="text" class="form-control" id="zglos_address" name="zglos_address" required>
                    </label>
                </div>
                <div class="col-xl-4 col-md-4 col-6">
                    <label>
                        Dodaj zdjęcie
                        <input type="file" class="form-control" id="zglos_image" name="zglos_image" required>
                    </label>
                </div>
                <div class="col-12 mb-3">
                    <div class="row">
                        <?php
                        $args = array(
                            'post_type' => 'kursy',                                
                            'order' => 'DESC',
                            'orderby' => 'post_date',
                            'posts_per_page' => -1,
                        );

                        $wp_query = new WP_Query($args);
                        $loop = new WP_Query($args);
                        while ($loop->have_posts()) : $loop->the_post();
                        ?>
                        
                        <div class="col-lg-3 label-box">
                            <label>
                                <input type="checkbox" id="zglos_kurs" name="zglos_kurs[]" value="<?php echo the_title(); ?>"> <?php echo the_title(); ?>
                            </label>
                        </div>
                        
                        <?php
                            endwhile; 
                            wp_reset_postdata();
                        ?>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4 col-6">
                    <label>
                        Adres email
                        <input type="email" class="form-control" id="zglos_email" name="zglos_email" required>
                    </label>
                </div>
                <div class="col-xl-4 col-md-4 col-6">
                    <label>
                        Numer telefonu
                        <input type="text" class="form-control" id="zglos_phone" name="zglos_phone" required>
                    </label>
                </div>
                <div class="col-xl-4 col-md-4 col-6 mt-3">
                    <div class="position-button">
                        <button type="submit" class="btn btn-primary w-100 mt-2" id="zglos_submit" name="zglos_submit">
                            Wyślij zgłoszenie
                        </button>
                    </div>
                </div>
            </div>
        </form>
        </div>
    </section>
    <section class="cta">
        <?php get_template_part( 'parts/question/question' ); ?>
    </section>
</main>
<?php get_footer(); ?>